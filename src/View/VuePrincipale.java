package View;

import Controller.*;
import Model.Music.ClicMusic;
import Model.Music.MainMusic;
import Model.ResizeImg;

import javax.swing.*;
import java.awt.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class VuePrincipale extends JFrame {
    public JFrame jf = new JFrame();

    public VuePrincipale() throws HeadlessException{
        try {//Paramètre de la fenêtre
            jf.setSize(950, 520);
            jf.setTitle("Accueil");
            jf.setResizable(true);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jf.setLocationRelativeTo(null);
            jf.setLayout(new GridBagLayout());

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.weightx = 1;
            gbc.weighty = 1;

            //Mise en place des différents panels
            JPanel finale = new JPanel();
            JPanel exfinale = new JPanel();
            JPanel panelButton = new JPanel();
            JPanel panelMode1 = new JPanel();
            JPanel panelMode2 = new JPanel();
            JPanel panelMode3 = new JPanel();
            JPanel panelMode4 = new JPanel();

            finale.setLayout(new BoxLayout(finale, BoxLayout.X_AXIS));
            exfinale.setLayout(new BoxLayout(exfinale, BoxLayout.Y_AXIS));
            panelButton.setLayout(new BoxLayout(panelButton, BoxLayout.Y_AXIS));
            panelMode1.setLayout(new BoxLayout(panelMode1, BoxLayout.X_AXIS));
            panelMode2.setLayout(new BoxLayout(panelMode2, BoxLayout.X_AXIS));
            panelMode3.setLayout(new BoxLayout(panelMode3, BoxLayout.X_AXIS));
            panelMode4.setLayout(new BoxLayout(panelMode4, BoxLayout.X_AXIS));

            //Création des éléments
            JLabel left = new JLabel(new ImageIcon(new File("Image\\left.PNG").getPath()));
            JLabel right = new JLabel(new ImageIcon(new File("Image\\right.PNG").getPath()));
            JLabel cartographers = new JLabel(new ImageIcon(new File("Image\\cartographers.PNG").getPath()));
            JLabel salutation = new JLabel("Bonjour, choisissez un mode de jeu :");
            JButton mode1 = new JButton("Mode Solo"); mode1.setContentAreaFilled(false);
            JButton mode2 = new JButton("Mode multijoueur local"); mode2.setContentAreaFilled(false);
            JButton mode3 = new JButton("Mode contre IA"); mode3.setContentAreaFilled(false);
            JButton mode4 = new JButton("Reprise d'une sauvegarde"); mode4.setContentAreaFilled(false);
            JButton quitter = new JButton("Quitter"); quitter.setContentAreaFilled(false);

            //Les comboBox
            String[] propositionNb = {"2","3","4","5"};
            String[] propositionIANb = {"1","2","3","4"};
            String[] propositionSaves=Controller.getSaves();
            JComboBox mode2JC = new JComboBox(propositionNb);
            JComboBox mode3JC = new JComboBox(propositionIANb);
            JComboBox mode4JC =new JComboBox(propositionSaves);
            JTextField mode1Input = new JTextField("                                    Indiquez le nom");

            //Ajout et mise en place
            panelMode1.add(mode1);panelMode1.add(Box.createRigidArea(new Dimension(50, 0)));panelMode1.add(mode1Input);
            panelMode2.add(mode2);panelMode2.add(Box.createRigidArea(new Dimension(50, 0)));panelMode2.add(mode2JC);
            panelMode3.add(mode3);panelMode3.add(Box.createRigidArea(new Dimension(50, 0)));panelMode3.add(mode3JC);
            panelMode4.add(mode4);panelMode4.add(Box.createRigidArea(new Dimension(50,0)));panelMode4.add(mode4JC);

            //Panel des bouttons
            panelButton.add(salutation);panelButton.add(Box.createRigidArea(new Dimension(0, 50)));
            panelButton.add(panelMode1);panelButton.add(Box.createRigidArea(new Dimension(0, 40)));
            panelButton.add(panelMode2);panelButton.add(Box.createRigidArea(new Dimension(0, 40)));
            panelButton.add(panelMode3);panelButton.add(Box.createRigidArea(new Dimension(0, 40)));
            panelButton.add(panelMode4);panelButton.add(Box.createRigidArea(new Dimension(0, 60)));
            panelButton.add(quitter);

            //Mise en forme
            cartographers.setAlignmentX(Component.CENTER_ALIGNMENT);
            salutation.setAlignmentX(Component.CENTER_ALIGNMENT);
            mode1.setAlignmentX(Component.CENTER_ALIGNMENT);
            mode2.setAlignmentX(Component.CENTER_ALIGNMENT);
            mode3.setAlignmentX(Component.CENTER_ALIGNMENT);
            mode4.setAlignmentX(Component.CENTER_ALIGNMENT);
            quitter.setAlignmentX(Component.CENTER_ALIGNMENT);

            //Evenements
            mode1.addActionListener( e -> {
                ClicSong();
                try { StartMode1(mode1Input.getText()); }
                catch (IOException ioException) { ioException.printStackTrace(); }
            } );

            mode2.addActionListener(e -> { ClicSong(); SelectMode2(mode2JC); } );

            mode3.addActionListener(e -> { ClicSong(); SelectMode3(mode3JC); } );

            mode4.addActionListener(e-> { ClicSong();
                try {
                    SelectMode4(mode4JC);
                } catch (IOException | ClassNotFoundException ioException) {
                    ioException.printStackTrace();
                }
            });

            quitter.addActionListener(e -> { ClicSong(); jf.dispose(); } );

            exfinale.add(cartographers);exfinale.add(Box.createRigidArea(new Dimension(0, 20)));
            exfinale.add(panelButton);

            finale.add(left); finale.add(Box.createRigidArea(new Dimension(20, 0)));
            finale.add(exfinale); finale.add(Box.createRigidArea(new Dimension(20, 0)));
            finale.add(right);

            gbc.gridx = 1;
            gbc.gridy = 1;
            jf.add(finale,gbc);
        }
        finally {
            jf.setVisible(true);
        }

    }

    /*
    Méthode pour que l'utilisateur choisse le nom et le titre des joueurs
     */
    public void SelectMode2(JComboBox comboBox){

        jf.dispose();

        int nbPlayers = Integer.parseInt((String) Objects.requireNonNull(comboBox.getSelectedItem()));//Récupération de la valeur
        //Mise en forme de la page
        JFrame selectWindow = new JFrame();
        selectWindow.setSize(500, 750);
        selectWindow.setTitle("A vous de chosir");
        selectWindow.setResizable(true);
        selectWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        selectWindow.setLocationRelativeTo(null);
        selectWindow.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 1;

        //Les listes de tous les élements en fonction du nombre passé en paramètre
        ArrayList<JLabel> jlabnom = new ArrayList<>();
        ArrayList<JLabel> jlabtitre = new ArrayList<>();
        ArrayList<JTextField> jtfnom = new ArrayList<>();
        ArrayList<JTextField> jtftitre = new ArrayList<>();
        ArrayList<JPanel> jpanom = new ArrayList<>();
        ArrayList<JPanel> jpatitre = new ArrayList<>();
        ArrayList<JPanel> jpatotal = new ArrayList<>();

        //panel quasi final
        JPanel totalSelect = new JPanel();
        totalSelect.setLayout(new BoxLayout(totalSelect,BoxLayout.Y_AXIS));

        for (int i=0 ; i<nbPlayers ; i++){


            //Partie panel : label espace textfield nom
            jlabnom.add(new JLabel("Nom du joueur "+(i+1)+" : "));
            jtfnom.add(new JTextField());
            jtfnom.get(i).setPreferredSize(new Dimension(120,30));
            jpanom.add(new JPanel());
            jpanom.get(i).setLayout(new BoxLayout(jpanom.get(i),BoxLayout.X_AXIS));
            jpanom.get(i).add(jlabnom.get(i));
            jpanom.get(i).add(Box.createRigidArea(new Dimension(20, 0)));
            jpanom.get(i).add(jtfnom.get(i));

            //Partie panel : label espace textfield titre
            jlabtitre.add(new JLabel("Titre du joueur "+(i+1) +" : "));
            jtftitre.add(new JTextField());
            jtftitre.get(i).setPreferredSize(new Dimension(120,30));
            jpatitre.add(new JPanel());
            jpatitre.get(i).setLayout(new BoxLayout(jpatitre.get(i),BoxLayout.X_AXIS));
            jpatitre.get(i).add(jlabtitre.get(i));
            jpatitre.get(i).add(Box.createRigidArea(new Dimension(20, 0)));
            jpatitre.get(i).add(jtftitre.get(i));

            //Partie ajout du total
            jpatotal.add(new JPanel());
            jpatotal.get(i).setLayout(new BoxLayout(jpatotal.get(i),BoxLayout.Y_AXIS));
            jpatotal.get(i).add(jpanom.get(i));
            jpatotal.get(i).add(Box.createRigidArea(new Dimension(0, 10)));
            jpatotal.get(i).add(jpatitre.get(i));


            //Ajout du panel total
            totalSelect.add(jpatotal.get(i));
            totalSelect.add(Box.createRigidArea(new Dimension(0, 30)));


        }
        //Mise en forme du panel final
        JButton reset = new JButton("Retour"); reset.setContentAreaFilled(false);
        JButton continuons = new JButton("Continuons"); continuons.setContentAreaFilled(false);
        JLabel lab = new JLabel("Faites vous plaisir !");
        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.X_AXIS));
        JPanel finale = new JPanel();
        finale.setLayout(new BoxLayout(finale,BoxLayout.Y_AXIS));

        //boutton reset -> retour en arrièere, bouton continuons -> début de la partie
        buttons.add(reset);
        buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        buttons.add(continuons);

        finale.add(lab); lab.setAlignmentX(Component.CENTER_ALIGNMENT);
        finale.add(Box.createRigidArea(new Dimension(0, 30)));
        finale.add(totalSelect);
        finale.add(Box.createRigidArea(new Dimension(0, 30)));
        finale.add(buttons); buttons.setAlignmentX(Component.CENTER_ALIGNMENT);

        //Evenements
        continuons.addActionListener(e -> { ClicSong(); selectWindow.dispose();
            try {
                StartMode2(jtfnom, jtftitre);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } );

        reset.addActionListener(e -> { ClicSong(); selectWindow.dispose(); JFrame v = new VuePrincipale(); } );

        gbc.gridx = 1;
        gbc.gridy = 1;
        selectWindow.add(finale,gbc);
        selectWindow.setVisible(true);
    }

    public void SelectMode3(JComboBox comboBox){
        jf.dispose();

        int nbPlayers = Integer.parseInt((String) Objects.requireNonNull(comboBox.getSelectedItem()));//Récupération de la valeur

        //Mise en forme de la page
        JFrame selectWindow = new JFrame();
        selectWindow.setSize(500, 500);
        selectWindow.setTitle("A vous de chosir");
        selectWindow.setResizable(true);
        selectWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        selectWindow.setLocationRelativeTo(null);
        selectWindow.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 1;

        JButton reset = new JButton("Retour"); reset.setContentAreaFilled(false);
        JButton continuons = new JButton("Continuons"); continuons.setContentAreaFilled(false);
        JLabel lab = new JLabel("Faites vous plaisir !");
        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.X_AXIS));
        JPanel finale = new JPanel();
        finale.setLayout(new BoxLayout(finale,BoxLayout.Y_AXIS));
        JPanel totalSelect = new JPanel();
        totalSelect.setLayout(new BoxLayout(totalSelect,BoxLayout.Y_AXIS));

        JPanel nameNom = new JPanel();nameNom.setLayout(new BoxLayout(nameNom,BoxLayout.X_AXIS));
        JPanel titleTitre = new JPanel();titleTitre.setLayout(new BoxLayout(titleTitre,BoxLayout.X_AXIS));

        JLabel name = new JLabel("Nom : ");
        JTextField nom = new JTextField();
        JLabel title = new JLabel("Titre : ");
        JTextField titre = new JTextField();

        nameNom.add(name); nameNom.add(Box.createRigidArea(new Dimension(30, 0))); nameNom.add(nom);
        titleTitre.add(title); titleTitre.add(Box.createRigidArea(new Dimension(30, 0))); titleTitre.add(titre);

        totalSelect.add(nameNom); totalSelect.add(Box.createRigidArea(new Dimension(0, 30))); totalSelect.add(titleTitre);

        //boutton reset -> retour en arrièere, bouton continuons -> début de la partie
        buttons.add(reset);
        buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        buttons.add(continuons);

        finale.add(lab); lab.setAlignmentX(Component.CENTER_ALIGNMENT);
        finale.add(Box.createRigidArea(new Dimension(0, 30)));
        finale.add(totalSelect);
        finale.add(Box.createRigidArea(new Dimension(0, 30)));
        finale.add(buttons); buttons.setAlignmentX(Component.CENTER_ALIGNMENT);



        //Evenements
        continuons.addActionListener(e -> { ClicSong(); selectWindow.dispose();
            try {
                StartMode3(nbPlayers, nom.getText(),titre.getText());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } );

        reset.addActionListener(e -> { ClicSong(); selectWindow.dispose(); JFrame v = new VuePrincipale(); } );

        gbc.gridx = 1;
        gbc.gridy = 1;
        selectWindow.add(finale,gbc);
        selectWindow.setVisible(true);
    }

    /*
    MÃ©thode pour que l'utilisateur choissise quelle sauvegarde charger
     */
    public void SelectMode4(JComboBox comboBox) throws IOException, ClassNotFoundException {
        jf.dispose(); //ferme la fenÃªtre
        Controller.chargerPartie(new File((String) Objects.requireNonNull(comboBox.getSelectedItem()))); //Charge la sauvegarde sÃ©lÃ©ctionnÃ©e
        StartMode4(); //Lance la partie chargÃ©e
    }

    /*
    Méthode pour le début de la partie en mode 1 (Solo)
     */
    public void StartMode1(String name) throws IOException {
        jf.dispose();

        var nom = new ArrayList<String>(); nom.add(name);

        Controller.StartWithNomSolo(nom);
        JFrame v = new Vue();

        Thread music = new MainMusic();
        music.start();
    }

    /*
    Méthode pour le début de la partie en mode 2 (Local multijoueur)
     */
    public void StartMode2(ArrayList<JTextField> noms, ArrayList<JTextField> titres ) throws IOException {

        ArrayList<String> nom = new ArrayList<>();
        ArrayList<String> titre = new ArrayList<>();

        for(int i=0 ; i<noms.size() ; i++){
            nom.add(noms.get(i).getText());
            titre.add(titres.get(i).getText());
        }

        Controller.StartWithNomMulti(nom,titre);
        JFrame v = new Vue();

        Thread music = new MainMusic();
        music.start();

    }

    public void StartMode3(int nbPlayer, String nom, String titre) throws IOException {

        Controller.StartWithNomIA(nbPlayer, nom, titre);
        JFrame v = new Vue();

        Thread music = new MainMusic();
        music.start();
    }
    /*
   MÃ©thode pour le dÃ©but de la partie en mode charger sauvegarde
    */
    public void StartMode4() throws IOException {
        JFrame v = new Vue();
        Thread music = new MainMusic();
        music.start();
    }

    public void ClicSong(){
        Thread music = new ClicMusic();
        music.start();
    }

    public static void main(String[] args) {
        Thread resizer = new ResizeImg();
        resizer.start();
        JFrame v = new VuePrincipale();
    }


}
