package View.Panels;

import javax.swing.JPanel;
import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class JPanelTransparent extends JPanel {


    BufferedImage mainImage = null;
    public void paint(Graphics g) {
        if (mainImage == null || mainImage.getWidth() != getWidth() || mainImage.getHeight() != getHeight()) {

            mainImage = (BufferedImage) createImage(getWidth(), getHeight());
        }

        Graphics2D g2 = mainImage.createGraphics();
        g2.setClip(g.getClip());
        super.paint(g2);
        g2.dispose();

        g2 = (Graphics2D) g.create();
        g2.setComposite(AlphaComposite.SrcOver.derive(0.5f));
        g2.drawImage(mainImage, 0, 0, null);
    }
}