package View;

import Controller.*;
import Controller.events.*;
import Model.Jeu.JeuSolo;
import Model.cartes.*;
import Model.maps.Matrice;
import Model.players.IA;
import Model.players.Joueur;
import View.Panels.JPanelTransparent;
import View.Panels.PanelImage;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import java.awt.*;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;


public class Vue extends JFrame {
    /*
    Différents éléments en attribut pour pouvoir les modifiers facilement depuis les événements
     */
    public static JFrame jf = new JFrame();
    public static JPanel map;
    public static GridBagConstraints gc;
    public static JPanel right;
    public static JPanel name;
    public static JPanel saisonDecret;
    public static JPanel score;


    public Vue() throws HeadlessException, IOException {
        //Récupération des tailles de l'ecran permettant après d'adapter la fenêtre
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        try {
            //Paramètres de base de la fenêtre
            jf.setSize((int) (width), (int) (height));
            jf.setTitle("Jeu Local");
            jf.setResizable(true);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jf.setLocationRelativeTo(null);
            JPanel window = new JPanel();
            window.setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();

            gbc.weightx = 1;
            gbc.weighty = 1;


            //Conteneur côté droit de la fenêtre
            right = new JPanel();right.setLayout(new BoxLayout(right,BoxLayout.Y_AXIS));
            name = new JPanel(); saisonDecret = new JPanel(); score = new JPanel();
            JPanel topRight = new JPanel();topRight.setLayout(new BoxLayout(topRight,BoxLayout.X_AXIS));

            right.setBackground(new Color(207,193,175));
            name.setBackground(new Color(207,193,175));
            topRight.setBackground(new Color(207,193,175));
            saisonDecret.setBackground(new Color(207,193,175));

            //ajout des éléments
            right.add(getCarteTour());
            topRight.add(getSaison());
            topRight.add(getDecrets());
            name.add(getJoueur());
            saisonDecret.add(topRight);
            score = getScore();


            //Mise en forme sur la fenêtre
            //1 -> La carte et les matrices de la carte + bouton rotate
            gbc.gridx = 5;
            gbc.gridy = 3;
            window.add(right,gbc);

            //2 -> La map du joueur
            gbc.gridx = 2;
            gbc.gridy = 3;
            var m = getMap();
            window.add(m,gbc);
            map = m;


            //3 -> Titre et nom du joueur
            gbc.gridx = 2;
            gbc.gridy = 1;
            window.add(name,gbc);

            //4 -> Carte saison + deux décrets
            gbc.gridx = 5;
            gbc.gridy = 1;
            window.add(saisonDecret,gbc);

            //4,5 -> Bouttons quitter
            gbc.gridx = 5;
            gbc.gridy = 0;
            window.add(bouttonsQuitter(),gbc);

            //5 -> Score du joueur
            gbc.gridx = 1;
            gbc.gridy = 3;
            window.add(score,gbc);

            // 5,5 -> Score du joueur
            gbc.gridx = 2;
            gbc.gridy = 0;
            JLabel cartographers = new JLabel( new ImageIcon(new File("Image\\cartographersUse.PNG").getPath()));
            window.add(cartographers,gbc);


            //6 -> bouton help et son évenement et le chrono
            gbc.gridx = 1;
            gbc.gridy = 0;
            JPanel aideAndChrono = new JPanel(); aideAndChrono.setLayout(new BoxLayout(aideAndChrono,BoxLayout.Y_AXIS));
            aideAndChrono.setBackground(new Color(207,193,175));
            JButton bt = new JButton("?"); bt.setPreferredSize(new Dimension((width*41)/1920,(height*15)/1080));
            JLabel chrono = new JLabel();
            bt.addActionListener(e -> {
               JFrame vueAide = new VueAide(); });

            final int[] heure = {0};
            final int[] minute = { 0 };
            final int[] seconde = { 0 };
            ActionListener tache_timer= e1 -> {
                seconde[0]++;
                if(seconde[0] ==60)
                {
                    seconde[0] =0;
                    minute[0]++;
                }
                if(minute[0] ==60)
                {
                    minute[0] =0;
                    heure[0]++;
                }
                chrono.setText("Durée : "+heure[0] +":"+ minute[0] +":"+ seconde[0]);/* rafraichir le label */
            };

            Timer t = new Timer(1000,tache_timer);
            t.start();

            aideAndChrono.add(bt);
            aideAndChrono.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
            aideAndChrono.add(chrono);

            window.add(aideAndChrono,gbc);
            window.setBackground(new Color(207,193,175));

            jf.setContentPane(window);
            jf.getContentPane().setBackground(new Color(207,193,175));
            gc = gbc;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            jf.setVisible(true);
        }

    }

    /*
     Méthode pour "rafraichir la page à la fin du tour d'un joueur, pour que ça soit l'affichage du joueur d'après
    */
    public static void repaintTour() throws IOException, InterruptedException {
        map.removeAll();
        right.removeAll();
        name.removeAll();
        saisonDecret.removeAll();
        score.removeAll();

        JPanel topRight = new JPanel();topRight.setLayout(new BoxLayout(topRight,BoxLayout.X_AXIS));
        topRight.setBackground(new Color(207,193,175));

        //ajout des éléments
        right.add(getCarteTour());
        topRight.add(getSaison());
        topRight.add(getDecrets());
        name.add(getJoueur());
        saisonDecret.add(topRight);
        score.add(getScore());
        map.add(getMap());

    }


    /*
    Retourner la panel de la map du joueur
    Paramètre : le numéro du joueur
     */
    public static JPanel getMap(){
        //initialisation
        JPanel global = new JPanel();global.setLayout(new BoxLayout(global,BoxLayout.Y_AXIS));
        global.setBackground(new Color(207,193,175));
        JPanel conteneur;JPanel unique;
        JLabel image;
        File fichier;

        //Affichage de la matrice de la map du joueur passé en en paramètre
        var matrice= Controller.getCurrent_Joueur().getMatrice_Joueur();
        var liste=matrice.getMatrice();
        for(int i=0 ; i<matrice.getLigne() ; i++){
            conteneur = new JPanel();//panel ligne
            for(int j=0 ; j<matrice.getColonne() ; j++){
                unique = new JPanel();
                fichier=Controller.icone(liste[i][j].getType());
                assert fichier != null;
                image =new JLabel(new ImageIcon(fichier.getPath()));
                unique.add(image);
                unique.setLayout(new BoxLayout(unique,BoxLayout.X_AXIS));
                unique.addMouseListener(new MonMouseListener(Controller.event,i,j));//Ajout de l'évènement pour la case
                conteneur.add(unique,BorderLayout.CENTER);
                conteneur.setLayout(new BoxLayout(conteneur,BoxLayout.X_AXIS));
            }
            global.add(conteneur);//ajouter le panel ligne au tout
        }
        return global;
    }


    /*
    Méthode pour piocher une carte et retourne l'affichage de la carte
    + les matrices qui devra être placer
     */
    public static JPanel getCarteTour() throws InterruptedException {
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        //initialisation
        Carte_De_Jeu carte = Controller.getCarte();//Carte de jeu au hasard

        JPanel global = new JPanel();//Panel des matrices de la carte
        JPanel plusButton = new JPanel();//Panel matrices + button
        JPanel SpaceAndTermine = new JPanel();//Panel pour la carte ruine
        JPanel aff = new JPanel();//Panel final (carte + matrices + boutton)
        var oui = new JPanel();//Panel des matrices à poser
        boolean impossible = false;
        int i;

        global.setBackground(new Color(207,193,175));
        plusButton.setBackground(new Color(207,193,175));
        SpaceAndTermine.setBackground(new Color(207,193,175));
        aff.setBackground(new Color(207,193,175));

        //Si c'est une carte Explo choix, alors il y a la même matrice de différents types
        global.add(Box.createRigidArea(new Dimension((width*50)/1920,0)));
        if(carte instanceof Carte_Exploration_Choix){
            i = 0;

            for (String type : ((Carte_Exploration_Choix) carte).getListChoix()) {

                if(!Controller.isPossible(carte.getM(),type)){//Si impossible de joueur le coup
                    Controller.Impossible();
                    carte = Controller.getCarte();
                    impossible = true;
                    break;
                }
                else {//Si c'est posssible faire un traitement en fonction de la carte normale
                    oui = getMatriceTour(carte.getM(), type);
                    oui.addMouseListener(new MyMouseListener(carte, i));
                    global.add(oui);
                    global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                    i++;
                }
            }
            if(impossible){//Si impossible de jouer faire le traitement pour des cases uniques
                for(String type : ((Carte_Exploration_Choix) carte).getListChoix()){
                    oui = getMatriceTour(carte.getM(), type);
                    oui.addMouseListener(new MyMouseListener(carte, i));
                    global.add(oui);
                    global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                    i++;
                }
            }

        }//Si c'est une carte Explo unique deux matrices du même type
        else if(carte instanceof Carte_Exploration_Unique) {
            i = 0;
            for(Matrice m : ((Carte_Exploration_Unique) carte).getListMatrice()){

                if(!Controller.isPossible(carte.getM(),"Choix")){
                    Controller.Impossible();
                    carte = Controller.getCarte();
                    impossible = true;
                    break;
                }
                else {
                    oui = getMatriceUnique(m, ((Carte_Exploration_Unique) carte).getType());
                    oui.addMouseListener(new MyMouseListener(carte, i));
                    global.add(oui);
                    global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                    i++;
                }
            }
            if(impossible){
                for(String type : ((Carte_Exploration_Choix) carte).getListChoix()){
                    oui = getMatriceTour(carte.getM(), type);
                    oui.addMouseListener(new MyMouseListener(carte, i));
                    global.add(oui);
                    global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                    i++;
                }
            }
        }//Si monstre ou ruine
        else {
            //Si c'est monstre
            if(carte.getM()!=null) {//Prendre la matrice
                if(!Controller.isPossible(carte.getM(),"Monstre")){
                    Controller.Impossible();
                    carte = Controller.getCarte();
                    impossible = true;
                    i = 0;
                    for(String type : ((Carte_Exploration_Choix) carte).getListChoix()){
                        oui = getMatriceTour(carte.getM(), type);
                        oui.addMouseListener(new MyMouseListener(carte, i));
                        global.add(oui);
                        global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                        i++;
                    }
                }
                else {
                    oui = getMatriceTour(carte.getM(), "Monstre");
                    oui.addMouseListener(new MyMouseListener(carte, 0));
                    global.add(oui);
                }
            }
        }
        global.setLayout(new BoxLayout(global,BoxLayout.X_AXIS));
        JLabel cartePicture =new JLabel(new ImageIcon(carte.getImage().getPath()));//Label de la carte
        //Bouton rotate
        plusButton.setLayout(new BorderLayout());
        if (Controller.getCurrent_Joueur() instanceof IA){//Si c'est le tour de L'IA, attendre un peu et faire le même traitement que dans "EvenEndTurn"

            JPanel onlyText = new JPanel();
            onlyText.setBackground(new Color(207,193,175));

            JLabel text = new JLabel("Voilà ce qu'une IA a joué, cliquez pour continuer");
            onlyText.add(text);SpaceAndTermine.add(onlyText);
            SpaceAndTermine.add(Box.createRigidArea(new Dimension(0,(height*50)/1080)));
            JPanel SpaceAndNext = new JPanel();SpaceAndNext.setLayout(new BoxLayout(SpaceAndNext,BoxLayout.X_AXIS)); SpaceAndNext.add(Box.createRigidArea(new Dimension(0,(height*50)/1080)));
            SpaceAndNext.setBackground(new Color(207,193,175));

            JButton next = new JButton("Continuer"); next.addMouseListener(new EventEndTurn()); SpaceAndNext.add(next); SpaceAndTermine.add(SpaceAndNext);
            SpaceAndTermine.setLayout(new BoxLayout(SpaceAndTermine,BoxLayout.Y_AXIS));

            aff.add(cartePicture); aff.add(Box.createRigidArea(new Dimension(0,(height*50)/1080))); aff.add(SpaceAndTermine);

        }

        else if(!(carte instanceof Carte_Ruine)){//Si ce n'est pas une carte ruine
            JPanel spaceAndButton = new JPanel(); spaceAndButton.setLayout(new BoxLayout(spaceAndButton,BoxLayout.Y_AXIS));//panel comportant un espace + boutton rotate
            JPanel doubleButton = new JPanel(); doubleButton.setLayout(new BoxLayout(doubleButton,BoxLayout.X_AXIS));
            spaceAndButton.setBackground(new Color(207,193,175));
            doubleButton.setBackground(new Color(207,193,175));

            Icon icon = new ImageIcon("Icone\\rotate.PNG");
            JButton button = new JButton(icon);
            button.addMouseListener(new Rotate(global, carte));//évènement relié aux boutons rotate

            Icon icon2 = new ImageIcon("Icone\\reverse.PNG");
            JButton button2 = new JButton(icon2);
            button2.addMouseListener(new Reverse(global, carte));//évènement relié aux boutons reverse

            doubleButton.add(button);doubleButton.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));doubleButton.add(button2);

            JPanel LabelAndSpace = new JPanel(); LabelAndSpace.setLayout(new BoxLayout(LabelAndSpace,BoxLayout.Y_AXIS));//Panel du label d'indication + espace
            JPanel SpaceAndLabel = new JPanel(); SpaceAndLabel.setLayout(new BoxLayout(SpaceAndLabel,BoxLayout.X_AXIS));
            LabelAndSpace.setBackground(new Color(207,193,175));
            SpaceAndLabel.setBackground(new Color(207,193,175));

            JLabel lab ;
            if(impossible){
                lab = new JLabel("<HTML> <p style='text-align:center;'> Aucun coup possible, cette carte est imposée  </p> </HTML>");
            }
            else {
                lab = new JLabel("<HTML> <p style='text-align:center;'> La zone sera placée sur votre carte à partir de la case <br> la plus en haut à gauche de la zone sélectionnée </p> </HTML>");
            }
            SpaceAndLabel.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));SpaceAndLabel.add(lab);
            LabelAndSpace.add(SpaceAndLabel); LabelAndSpace.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));//Mise en forme du panel LabelSpace

            //Mise en forme
            plusButton.add(LabelAndSpace,BorderLayout.NORTH);
            plusButton.add(global, BorderLayout.CENTER);

            spaceAndButton.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));spaceAndButton.add(doubleButton);doubleButton.setAlignmentX(CENTER_ALIGNMENT);//Ajout des éléments
            plusButton.add(spaceAndButton, BorderLayout.SOUTH);//Ajout du panel bouton + space

            aff.add(cartePicture); aff.add(Box.createRigidArea(new Dimension(0,(height*50)/1080)));aff.add(plusButton);
        }
        else {//Si c'est une carte ruine
            JPanel onlyText = new JPanel();
            onlyText.setBackground(new Color(207,193,175));

            JLabel text = new JLabel("<HTML> <p style='text-align:center;'> Carte ruine : Lors de votre prochain tour vous devrez placer<br> la zone sur une case ruine </p> </HTML>");
            onlyText.add(text);SpaceAndTermine.add(onlyText);
            SpaceAndTermine.add(Box.createRigidArea(new Dimension(0,(height*50)/1080)));
            JPanel SpaceAndNext = new JPanel();SpaceAndNext.setLayout(new BoxLayout(SpaceAndNext,BoxLayout.X_AXIS)); SpaceAndNext.add(Box.createRigidArea(new Dimension(0,(height*50)/1080)));
            SpaceAndNext.setBackground(new Color(207,193,175));

            JButton next = new JButton("Continuer"); next.addMouseListener(new EventEndTurn()); SpaceAndNext.add(next); SpaceAndTermine.add(SpaceAndNext);
            SpaceAndTermine.setLayout(new BoxLayout(SpaceAndTermine,BoxLayout.Y_AXIS));

            aff.add(cartePicture); aff.add(Box.createRigidArea(new Dimension(0,(height*50)/1080))); aff.add(SpaceAndTermine);
        }


        return aff;
    }

    /*
    Méthode pour retourner l'affichage de Matrice
     */
    public static JPanel getMatriceTour(Matrice matricee, String type){
        //Initialisation
        JPanel conteneur = new JPanel();// Panel des lignes de la matrice
        JLabel image;//Label de chaque image
        File fichier;
        JPanel leftIn = new JPanelTransparent();//Panel calque (quasi transparent) final (retourne la matrice)
        leftIn.setLayout(new BoxLayout(leftIn,BoxLayout.Y_AXIS));

            for(int i=0 ; i<matricee.getLigne() ; i++){
                conteneur = new JPanel();//panel ligne
                for(int j=0 ; j<matricee.getColonne() ; j++){
                    if(matricee.getMatrice()[i][j].getType().equals("Vide")){
                        fichier = new File("Icone\\TransparantUse.PNG");//Si c'est vide alors l'image transparante
                    }
                    else {
                        fichier = new File("Icone\\"+type+"Use.PNG");//Sinon le type donnée
                    }
                    image =new JLabel(new ImageIcon(fichier.getPath()));//Récupération de l'image et affectation au panel ensuite
                    conteneur.add(image,BorderLayout.CENTER);
                    conteneur.setLayout(new BoxLayout(conteneur,BoxLayout.X_AXIS));
                }
                leftIn.add(conteneur);//ajouter le panel ligne au tout
                conteneur.setBackground(new Color(207,193,175));
            }

        conteneur.setBackground(new Color(207,193,175));
        leftIn.setBackground(new Color(207,193,175));

        return leftIn;
    }

    /*
    Méthode pour retourner l'affichage de Matrice en fonction de la matrice passer en paramètre
    */
    public static JPanel getMatriceUnique(Matrice matricee, String type){
        JPanel conteneur;// Panel des lignes
        JLabel image;//Label de chaque l'image
        File fichier;
        JPanel leftIn = new JPanelTransparent();//Panel calque (plus transparent) final
        leftIn.setLayout(new BoxLayout(leftIn,BoxLayout.Y_AXIS));

        for(int i=0 ; i<matricee.getLigne() ; i++){
            conteneur = new JPanel();//panel ligne
            for(int j=0 ; j<matricee.getColonne() ; j++){
                if(matricee.getMatrice()[i][j].getType().equals("Vide")){
                    fichier = new File("Icone\\TransparantUse.PNG");//Si la case est vide alors l'image transparente
                }
                else {
                    fichier = new File("Icone\\"+type+"Use.PNG");//Sinon l'image du type
                }
                image =new JLabel(new ImageIcon(fichier.getPath()));//Récupération de l'image et affactation
                conteneur.add(image,BorderLayout.CENTER);
                conteneur.setLayout(new BoxLayout(conteneur,BoxLayout.X_AXIS));
            }
            leftIn.add(conteneur);//ajouter le panel ligne au tout
            conteneur.setBackground(new Color(207,193,175));
        }

        leftIn.setBackground(new Color(207,193,175));

        return leftIn;
    }

    /*
    Retourne la saison actuelle
     */
    public static JPanel getSaison(){
        JPanel conteneur;//Panel final
        JLabel image;// Label de l'image

        Saison s=Controller.getCurrent_saison();//Récupère la saison actuelle
        image=new JLabel(new ImageIcon(s.getImage().getPath()));//Récupération de l'image de la carte saison
        conteneur=new JPanel();
        conteneur.add(image,BorderLayout.EAST);
        conteneur.setBackground(new Color(207,193,175));

        return conteneur;
    }

    /*
    Retourne les décrets de la saison actuelle
     */
    public static JPanel getDecrets(){
        JPanel conteneur=new JPanel();
        conteneur.setBackground(new Color(207,193,175));
        JLabel image;
        Saison s = Controller.getCurrent_saison();
        assert !s.getDecrets().isEmpty();//Vérification que les décrets ne sont pas vide

        for(Decret d : s.getDecrets()){// Pour tout les décrets
            image=new JLabel(new ImageIcon(d.getImage().getPath()));//Récupération de l'image des cartes
            conteneur.add(image);
        }

        conteneur.addMouseListener( new DecretEvents());

        return conteneur;

    }

    /*
    Retourne le nom et le titre du joueur
     */
    public static JPanel getJoueur() {
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        //Récupération des valeurs
        Joueur j = Controller.getCurrent_Joueur();
        Saison saison = Controller.getCurrent_saison();
        int numTour = Controller.getNbTour();
        JLabel titre = new JLabel("");

        //Création des panels
        JPanel global = new JPanel();//Panel final (nom + titre)
        global.setBackground(new Color(207,193,175));
        JPanel NomAndTitre = new JPanel();
        NomAndTitre.setBackground(new Color(207,193,175));
        global.setLayout(new BoxLayout(global,BoxLayout.Y_AXIS));
        NomAndTitre.setLayout(new BoxLayout(NomAndTitre,BoxLayout.X_AXIS));

        //Bordure du classement
        Border bordureJoueur = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
        NomAndTitre.setBorder(bordureJoueur);

        int indice = 0;
        for(int i=0 ; i<Controller.getJoueurs().size() ; i++){
            if(Controller.getJoueurs().get(i).equals(Controller.getCurrent_Joueur())){
                indice = i;
                break;
            }
        }

        //Création des labels
        JLabel temps = new JLabel("Temps restant de la saison : "+(saison.getDureeSaison()-numTour)+"/"+saison.getDureeSaison()); temps.setFont(new Font("Arial",Font.BOLD,(width*30)/1920));
        JLabel tour = new JLabel("Au tour du joueur "+(indice+1)); tour.setFont(new Font("Arial",Font.BOLD,(width*30)/1920));
        JLabel nom = new JLabel("Nom : "+j.getNom()); nom.setFont(new Font("Arial",Font.BOLD,(width*20)/1920));
        if(!(Controller.getJeu() instanceof JeuSolo)){
            titre = new JLabel("Titre : "+j.getTitre()); titre.setFont(new Font("Arial",Font.BOLD,(width*20)/1920));
        }


        //Ajout des éléments
        NomAndTitre.add(Box.createRigidArea(new Dimension((width*100)/1920, 0)));
        NomAndTitre.add(nom);
        NomAndTitre.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
        NomAndTitre.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));
        if(!(Controller.getJeu() instanceof JeuSolo)) {
            NomAndTitre.add(titre);
        }
        NomAndTitre.add(Box.createRigidArea(new Dimension((width*100)/1920, 0)));

        global.add(temps);temps.setAlignmentX(Component.CENTER_ALIGNMENT);
        global.add(Box.createRigidArea(new Dimension(0, (height*100)/1080)));
        global.add(tour);tour.setAlignmentX(Component.CENTER_ALIGNMENT);
        global.add(Box.createRigidArea(new Dimension(0, (height*60)/1080)));
        global.add(NomAndTitre);NomAndTitre.setAlignmentX(Component.CENTER_ALIGNMENT);


        return global;

    }

    /*
    Affichage de l'ecran de résultat
     */
    public static void endGame(){
        jf.dispose();

        JFrame scoreWindow = new JFrame();
        scoreWindow.setSize(550, 600);
        scoreWindow.setTitle("Résultats");
        scoreWindow.setResizable(true);
        scoreWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        scoreWindow.setLocationRelativeTo(null);

        JPanel finale = new JPanel();
        JPanel exfinale = new JPanel();
        exfinale.setLayout(new BoxLayout(exfinale, BoxLayout.Y_AXIS));
        finale.setLayout(new BoxLayout(finale, BoxLayout.Y_AXIS));

        JLabel cartographers = new JLabel(new ImageIcon(new File("Image\\cartographers.PNG").getPath()));
        exfinale.add(Box.createRigidArea(new Dimension(0, 20)));
        exfinale.add(cartographers);cartographers.setAlignmentX(Component.CENTER_ALIGNMENT);
        finale.add(Box.createRigidArea(new Dimension(0, 30)));

        if(Controller.getJeu() instanceof JeuSolo){
            scoreWindow.setSize(500, 400);

            JLabel fin = new JLabel("Partie terminée ! ");
            JLabel score = new JLabel("Votre score : "+Controller.getCurrent_Joueur().getScore_fin());
            JLabel titre = new JLabel("La reine Gimnax vous remet le titre :");
            JLabel titreFinale = new JLabel("\""+Controller.getCurrent_Joueur().getTitre()+"\"");

            fin.setFont(new Font("Arial", Font.BOLD, 35));
            score.setFont(new Font("Arial", Font.BOLD, 20));
            titre.setFont(new Font("Arial", Font.BOLD, 20));
            titreFinale.setFont(new Font("Arial", Font.BOLD, 20));

            fin.setAlignmentX(Component.CENTER_ALIGNMENT);
            score.setAlignmentX(Component.CENTER_ALIGNMENT);
            titre.setAlignmentX(Component.CENTER_ALIGNMENT);
            titreFinale.setAlignmentX(Component.CENTER_ALIGNMENT);

            finale.add(fin);
            finale.add(Box.createRigidArea(new Dimension(0, 50)));
            finale.add(score);
            finale.add(Box.createRigidArea(new Dimension(0, 50)));
            finale.add(titre);
            finale.add(titreFinale);

        }
        else {
            //Création des panels
            JPanel classement = new JPanel();
            JPanel spacesAndClassement = new JPanel();

            //Mise en forme des panels
            classement.setLayout(new BoxLayout(classement, BoxLayout.X_AXIS));
            spacesAndClassement.setLayout(new BoxLayout(spacesAndClassement, BoxLayout.Y_AXIS));

            //Bordure du classement
            Border bordureClassment = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
            spacesAndClassement.setBorder(bordureClassment);

            //Ajout des résultats dans le classement
            var resultats = Controller.getJoueurs();
            for (int i = 0; i < resultats.size(); i++) {
                classement = new JPanel();
                classement.setLayout(new BoxLayout(classement, BoxLayout.X_AXIS));
                spacesAndClassement.add(Box.createRigidArea(new Dimension(0, 30)));
                JLabel res = new JLabel((i + 1) + "   " + resultats.get(i).getNom() + "   " + resultats.get(i).getTitre() + " " + resultats.get(i).getScore_fin() + " points");
                res.setFont(new Font("Arial", Font.BOLD, 25));
                classement.add(Box.createRigidArea(new Dimension(50, 0)));
                classement.add(res);
                classement.add(Box.createRigidArea(new Dimension(50, 0)));
                spacesAndClassement.add(classement);
            }

            spacesAndClassement.add(Box.createRigidArea(new Dimension(0, 30)));
            JLabel vainqueur = new JLabel(resultats.get(0).getNom() + " remporte la partie !");
            vainqueur.setFont(new Font("Arial", Font.BOLD, 35));
            JLabel motClassement = new JLabel("Classement : ");

            JButton fin = new JButton("Quitter");
            fin.setContentAreaFilled(false);

            //Mise en place
            vainqueur.setAlignmentX(Component.CENTER_ALIGNMENT);
            motClassement.setAlignmentX(Component.CENTER_ALIGNMENT);
            spacesAndClassement.setAlignmentX(Component.CENTER_ALIGNMENT);
            fin.setAlignmentX(Component.CENTER_ALIGNMENT);

            //Ajouts finaux
            finale.add(Box.createRigidArea(new Dimension(0, 30)));
            finale.add(vainqueur);
            finale.add(Box.createRigidArea(new Dimension(0, 30)));
            finale.add(motClassement);
            finale.add(Box.createRigidArea(new Dimension(0, 30)));
            finale.add(spacesAndClassement);
            finale.add(Box.createRigidArea(new Dimension(0, 30)));
            finale.add(fin);

            //Evenement
            fin.addActionListener(e -> System.exit(0));
        }

        exfinale.add(finale);
        scoreWindow.setContentPane(exfinale);
        scoreWindow.setVisible(true);
    }

    /*
   Retourne le score de la saison qui vient de se terminer
    */
    public static JPanel getScore() throws IOException {
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        Joueur j = Controller.getCurrent_Joueur();

        JPanel global = new PanelImage(new File("Image\\Score.PNG").getPath());
        global.setLayout(new BoxLayout(global,BoxLayout.Y_AXIS));

        JLabel s00 = new JLabel("  "+j.getScore_Or().get(0)+"     "+j.getScore_Decret()[0][0]+"   ");
        JLabel s01 = new JLabel("  "+j.getScore_Gobelin().get(0)+"     "+j.getScore_Decret()[0][1]+"   ");
        JLabel s02 = new JLabel("      "+j.getScore().get(0)+"   ");

        JLabel s10 = new JLabel("  "+j.getScore_Or().get(1)+"     "+j.getScore_Decret()[1][0]+"   ");
        JLabel s11 = new JLabel("  "+j.getScore_Gobelin().get(1)+"     "+j.getScore_Decret()[1][1]+"   ");
        JLabel s12 = new JLabel("      "+j.getScore().get(1)+"   ");

        JLabel s20 = new JLabel("  "+j.getScore_Or().get(2)+"     "+j.getScore_Decret()[2][0]+"   ");
        JLabel s21 = new JLabel("  "+j.getScore_Gobelin().get(2)+"     "+j.getScore_Decret()[2][1]+"   ");
        JLabel s22 = new JLabel("      "+j.getScore().get(2)+"   ");

        JLabel s30 = new JLabel("  "+j.getScore_Or().get(3)+"     "+j.getScore_Decret()[3][0]+"   ");
        JLabel s31 = new JLabel("  "+j.getScore_Gobelin().get(3)+"     "+j.getScore_Decret()[3][1]+"   ");
        JLabel s32 = new JLabel("      "+j.getScore().get(3)+"   ");

        s00.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s01.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s02.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s10.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s11.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s12.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s20.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s21.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s22.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s30.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s31.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));
        s32.setFont(new Font("Arial",Font.BOLD,(width*22)/1920));

        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s00);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s01);
        global.add(Box.createRigidArea(new Dimension(0, (height*15)/1080)));
        global.add(s02);
        global.add(Box.createRigidArea(new Dimension(0, (height*43)/1080)));

        global.add(s10);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s11);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s12);
        global.add(Box.createRigidArea(new Dimension(0, (height*40)/1080)));

        global.add(s20);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s21);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s22);
        global.add(Box.createRigidArea(new Dimension(0, (height*40)/1080)));

        global.add(s30);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s31);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));
        global.add(s32);
        global.add(Box.createRigidArea(new Dimension(0, (height*10)/1080)));

        global.setBackground(new Color(207,193,175));

        return global;
    }

    public static JPanel bouttonsQuitter(){
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        JPanel global = new JPanel();

        global.setBackground(new Color(207,193,175));
        global.setLayout(new BoxLayout(global,BoxLayout.X_AXIS));

        JButton quitter = new JButton("Quitter");
        JButton quitterSave = new JButton("En sauvegardant");

        quitter.addActionListener( e -> System.exit(0) );
        quitterSave.addActionListener(e-> {
            try {
                Controller.savePartie();
                System.exit(0);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        global.add(quitter);
        global.add(Box.createRigidArea(new Dimension((width*10)/1920, 0)));
        global.add(quitterSave);

        return global;

    }

}