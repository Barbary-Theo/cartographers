package View;

import javax.swing.*;
import java.awt.*;


public class VueAide extends JFrame {
    public static JFrame ja = new JFrame();

    public VueAide() throws HeadlessException {

        try {
            ja.setSize(950, 400);
            ja.setTitle("Aide");
            ja.setResizable(false);
            ja.setLocationRelativeTo(null);

            JPanel jp = new JPanel();//Panel du text
            JLabel aideLabel1 = new JLabel("<HTML> <p style='text-align : center;'> Voici les régles du jeu : </p>" +
                    " <p>Vous avez une map (en bas à gauche), et vous pouvez placer différentes zones (partie de droite).<br>" +
                    "A vous de choisir l'emplacement de ces dernières sur votre map en fonction des points rapportés <br>" +
                    "par les décrets représentés en haut à droite. Celui qui a le plus de points à la fin de la partie, gagne ...<br> <br> </p> " +
                    "<p style='text-align : center;'> Informations complémentaires : </p> " +
                    "<p>- Il y a quatres saisons différentes avant la fin de la partie avec chacune deux décrets différents.<br>" +
                    "- Le nombre de tour de chaque saison est représenté par le sablier sur la carte saison (en haut à droite).<br>" +
                    "- Impossible de placer des zones monstres sur des zones ruines.<br> " +
                    "- Si vous ne pouvez plus placer de zone sur votre map, vous devrez placer des zones uniques de n'importe quel type.<br>" +
                    "- Après avoir eu une carte ruine, vous devrez placer la zone de la carte d'après sur une case ruine.<br>" +
                    "- Si vous ne pouvez pas placer de zone sur une case ruine après une carte ruine, vous devrez placer des zones uniques de n'importe quel type.<br>" +
                    "- Si vous souhaitez regarder la description des décrets placer votre souris sur les cartes décrets (en haut à droite).<br>" +
                    "- Lorsque vous placez une zone, c'est la case la plus en haut à gauche qui est placé à partir de l'endroit où vous avez cliqué.<br>" +
                    "- Vous pouvez tourner les zones à poser grâce au bouton de rotation.<br>" +
                    "- Vous pouvez renverser les zones à poser grâce au bouton reverse.<br>" +
                    "- Entourer une case montagne sur ses quatres côtés vous fera gagner une pièce d'or.<br>" +
                    "- Les cases gobelins retirent des points. </p> </HTML>"
            );
            jp.add(aideLabel1);
            aideLabel1.setAlignmentX(Component.CENTER_ALIGNMENT);

            JButton ok = new JButton("Ok !");
            ok.addActionListener(e -> ja.dispose());//Si appuyé sur le boutton, fermer la page d'aide

            JPanel all = new JPanel();
            all.setLayout(new BoxLayout(all, BoxLayout.Y_AXIS));//Panel total
            all.add(jp);
            all.add(ok);
            all.add(Box.createRigidArea(new Dimension(0, 50)));

            ja.add(all);

        }finally {
            ja.setVisible(true);
        }

    }


}
