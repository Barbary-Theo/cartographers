package Model.Music;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;


import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;




public class MonstreMusic extends Thread {

    public void run() {
        playMusic("Song\\Monstre.wav");
    }

    //Pour mettre de la musique lancer cette fonction avec chemin d'acces en parametre
    public static void playMusic(String filepath) {
        AudioInputStream audioInputStream;
        try{
            audioInputStream = AudioSystem.getAudioInputStream(new File(filepath));
            BufferedInputStream bufferedInputStream = new BufferedInputStream(audioInputStream);
            audioInputStream = new AudioInputStream(bufferedInputStream, audioInputStream.getFormat(), audioInputStream.getFrameLength());
        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
            return;
        }
        AudioFormat format = audioInputStream.getFormat();
        DataLine.Info info = new DataLine.Info(SourceDataLine.class,format);
        SourceDataLine line;
        try {
            line = (SourceDataLine) AudioSystem.getLine(info);

        } catch (LineUnavailableException e) {
            e.printStackTrace();
            return;
        }
        try {
            line.open(format);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
            return;
        }
        line.start();
        try {
            byte[] bytes = new byte[1024];
            int bytesRead;
            while ((bytesRead = audioInputStream.read(bytes, 0, bytes.length)) != -1) {
                line.write(bytes, 0, bytesRead);
            }
        } catch (IOException io) {
            io.printStackTrace();
            return;
        }
        line.close();
    }

}