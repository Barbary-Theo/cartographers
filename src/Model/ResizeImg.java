package Model;

import java.awt.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;
import java.util.ArrayList;

public class ResizeImg extends Thread
{
    public static void changeSize(String inImg, String outImg, int w, int h)
            throws IOException
    {
        // lit l'image d'entrée
        File f = new File(inImg);
        BufferedImage inputImage = ImageIO.read(f);

        // crée l'image de sortie
        BufferedImage img = new BufferedImage(w, h, inputImage.getType());

        // balancer l'image d'entrée à l'image de sortie
        Graphics2D g = img.createGraphics();
        g.drawImage(inputImage, 0, 0, w, h, null);
        g.dispose();

        // extrait l'extension du fichier de sortie
        String name = outImg.substring(outImg.lastIndexOf(".") + 1);

        // écrit dans le fichier de sortie
        ImageIO.write(img, name, new File(outImg));
    }

    public void run()
    {

        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        //Partie zone unique(Bois, Eau, Ferme, ...)
        //ArrayList<String> e = new ArrayList<>(); e.add("Icone\\Transparant.png");e.add("Icone\\Bois.png");e.add("Icone\\Eau.png");e.add("Icone\\Ferme.png");e.add("Icone\\Maison.png");e.add("Icone\\Monstre.png");e.add("Icone\\Montagne.png");e.add("Icone\\Temple.png");e.add("Icone\\Vide.png");
        //e.add("Icone\\reverse.png"); e.add("Icone\\rotate.png");
        String chemin0="Icone";  //Chemin du dossier des cartes
        File p0=new File(chemin0);
        File[] dossier0=p0.listFiles();
        int Zwidth = (width*52)/1920;
        int Zheight = (height*52)/1080;

        String chemin="Cartes\\Exploration\\Uniques";  //Chemin du dossier des cartes
        File p=new File(chemin);
        File[] dossier=p.listFiles(); //Liste contenant tous les dossiers du fichier dans le chemin de la variable chemin
        String chemin2="Cartes\\Exploration\\Choix";
        File p2=new File(chemin2);
        File[] dossier2=p2.listFiles();
        String chemin3="Cartes\\Monstres";
        File p3=new File(chemin3);
        File[] dossier3=p3.listFiles();
        String chemin4="Cartes\\Ruines";
        File p4=new File(chemin4);
        File[] dossier4=p4.listFiles();
        String chemin5="Cartes\\Decret";
        File p5=new File(chemin5);
        File[] dossier5=p5.listFiles();
        String chemin6="Cartes\\Saison";
        File p6=new File(chemin6);
        File[] dossier6=p6.listFiles();
        int Uwidth = (width*288)/1920;
        int Uheight = (height*388)/1080;
        int Twidth = (width*240)/1920;
        int Theight = (height*340)/1080;

        try
        {
            if(verify(new File("Image\\Score.PNG"))) ResizeImg.changeSize("Image\\Score.PNG","Image\\ScoreUse.PNG",(width*72)/1920,(height*528)/1080);
            if(verify(new File("Image\\cartographers.PNG"))) ResizeImg.changeSize("Image\\cartographers.PNG","Image\\cartographersUse.PNG",(width*500)/1920,(height*60)/1080);

            assert dossier0 != null;
            for (File f : dossier0) {
                if(verify(f)) ResizeImg.changeSize("Icone\\" + f.getName(), "Icone\\" + ChangeName(f.getName()), Zwidth, Zheight);
            }
            assert dossier != null;
            for (File f : dossier) {
                if(verify(f)) ResizeImg.changeSize("Cartes\\Exploration\\Uniques\\" + f.getName(), "Cartes\\Exploration\\Uniques\\" + ChangeName(f.getName()), Uwidth, Uheight);
            }
            assert dossier2 != null;
            for (File f : dossier2) {
                if(verify(f)) ResizeImg.changeSize("Cartes\\Exploration\\Choix\\" + f.getName(), "Cartes\\Exploration\\Choix\\" + ChangeName(f.getName()), Uwidth, Uheight);
            }
            assert dossier3 != null;
            for (File f : dossier3) {
                if(verify(f)) ResizeImg.changeSize("Cartes\\Monstres\\" + f.getName(), "Cartes\\Monstres\\" + ChangeName(f.getName()), Uwidth, Uheight);
            }
            assert dossier4 != null;
            for (File f : dossier4) {
                if(verify(f)) ResizeImg.changeSize("Cartes\\Ruines\\" + f.getName(), "Cartes\\Ruines\\" + ChangeName(f.getName()), Uwidth, Uheight);
            }
            assert dossier5 != null;
            for (File f : dossier5) {
                if(verify(f)) ResizeImg.changeSize("Cartes\\Decret\\" + f.getName(), "Cartes\\Decret\\" + ChangeName(f.getName()), Twidth, Theight);
            }
            assert dossier6 != null;
            for (File f : dossier6) {
                if(verify(f)) ResizeImg.changeSize("Cartes\\Saison\\" + f.getName(), "Cartes\\Saison\\" + ChangeName(f.getName()), Twidth, Theight);
            }

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }


    /*
    Méthode pour changer le nom d'un fichier en ajoutant le préfixe "Use"
     */
    public static String ChangeName(String val){

        String start = "";
        String extenssion = "";
        boolean before = false;

        for(int i=0 ; i<val.length() ; i++){//Pour toutes les lettres du mot
            if(!before && val.charAt(i)!='.'){//Si la lette n'est pas égale à un point ou ne l'a pas déjà été
                start = start+val.charAt(i);//Alors j'ajoute la lettre à la variable "start"
            }
            else {//Sinon
                before = true;//J'indique qu'on est déjà passé par le point
                extenssion = extenssion+val.charAt(i);//et donc je récupère les lettres de l'extenssion
            }
        }

        start = start+"Use"+extenssion;//J'indique que le mot start est maintenant : le nom de base + le mose Use + l'extenssion



        return start;
    }

    public static boolean verify(File fichier){


        for (int i=0 ; i<fichier.getName().length() ; i++){

            if(fichier.getName().charAt(i)=='U' && fichier.getName().charAt(i+1)=='s' && fichier.getName().charAt(i+2)=='e'){
                return false;
            }
            if(fichier.getName().charAt(i)=='.'){
                break;
            }

        }

        return true;
    }

}