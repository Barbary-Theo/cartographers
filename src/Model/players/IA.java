package Model.players;

import Controller.Controller;
import Model.Jeu.Jeu;
import Model.cartes.*;
import Model.maps.Case;
import Model.maps.Matrice;

import java.util.Random;

public class IA extends Joueur {

    public IA(String nom, String titre) {
        super(nom, titre);
    }

    public void poserZoneIA (Carte_De_Jeu carte){

        int saison = Jeu.getInstance().getCurrent_saison().getNumSaison();

        int score = 0;
        int x = 0;
        int y = 0;
        int numMatrice = 0;
        int numRotate = 0;

        if(carte instanceof Carte_Exploration_Unique){//carte même type

            for (int i=0 ; i<matrice_Joueur.getLigne() ; i++){
                for (int j=0 ; j<matrice_Joueur.getColonne() ; j++){

                    int num = 0;//correspond au numéro de la matrice à poser
                    for(Matrice m : ((Carte_Exploration_Unique) carte).getListMatrice()){
                        //Pour toutes les rotations
                        int k = 0;
                        while (k<4){
                            //Récupération du score actuel
                            int decretA = score_Decret[saison][0];
                            int decretB = score_Decret[saison][1];
                            int or = score_Or.get(saison);
                            int gobelin = score_Gobelin.get(saison);

                            poserZoneUnique(m,((Carte_Exploration_Unique) carte).getType(), i, j);

                            //recalcul des scores
                            scoreOr();
                            scoreGobelin();
                            scoreDecret();

                            if(score<score_Decret[saison][0]+score_Decret[saison][1]+score_Or.get(saison)-score_Gobelin.get(saison)){
                                x = i; y = j; score = score_Decret[saison][0]+score_Decret[saison][1]+score_Or.get(saison)-score_Gobelin.get(saison); numMatrice = num; numRotate = k;
                            }

                            //Reset des scores et map
                            score_Decret[saison][0] = decretA;
                            score_Decret[saison][1] = decretB;
                            score_Or.set(saison,or);
                            score_Gobelin.set(saison,gobelin);
                            setMatrice_Joueur(ex_matrice_Joueur);

                            m.rotateCarte();
                            k++;
                        }

                        num++;
                    }

                }
            }

            Matrice m = new Matrice(((Carte_Exploration_Unique) carte).getListMatrice().get(numMatrice));
            int rotate = 0;
            while(rotate<numRotate){
                m.rotateCarte();
                rotate++;
            }
            if(!poserZoneUnique(m,((Carte_Exploration_Unique) carte).getType(),x,y)){
                noIdea(carte);
            }
            else {
                poserZoneUnique(m,((Carte_Exploration_Unique) carte).getType(),x,y);
            }

        }
        else if (carte instanceof Carte_Exploration_Choix) {//carte différents types
            Matrice m = new Matrice(carte.getM());
            for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                    int num = 0;//correspond au numéro de la matrice à poser
                    for (String type : ((Carte_Exploration_Choix) carte).getListChoix()) {
                        //Pour toutes les rotations
                        int k = 0;
                        while (k < 4) {
                            //Récupération du score actuel
                            int decretA = score_Decret[saison][0];
                            int decretB = score_Decret[saison][1];
                            int or = score_Or.get(saison);
                            int gobelin = score_Gobelin.get(saison);

                            poserZoneUnique(m, type, i, j);

                            //recalcul des scores
                            scoreOr();
                            scoreGobelin();
                            scoreDecret();

                            if (score < score_Decret[saison][0] + score_Decret[saison][1] + score_Or.get(saison) - score_Gobelin.get(saison)) {
                                x = i;
                                y = j;
                                score = score_Decret[saison][0] + score_Decret[saison][1] + score_Or.get(saison) - score_Gobelin.get(saison);
                                numMatrice = num;
                                numRotate = k;
                            }

                            //Reset des scores et map
                            score_Decret[saison][0] = decretA;
                            score_Decret[saison][1] = decretB;
                            score_Or.set(saison, or);
                            score_Gobelin.set(saison, gobelin);
                            setMatrice_Joueur(ex_matrice_Joueur);

                            m.rotateCarte();
                            k++;
                        }

                        num++;
                    }

                }
            }

            m = new Matrice(carte.getM());
            int rotate = 0;
            while(rotate<numRotate){
                m.rotateCarte();
                rotate++;
            }
            if(! poserZoneUnique(m,((Carte_Exploration_Choix) carte).getListChoix().get(numMatrice),x,y)){
                noIdea(carte);
            }
            else {
                poserZoneUnique(m,((Carte_Exploration_Choix) carte).getListChoix().get(numMatrice),x,y);
            }
        }
        else if(carte instanceof Carte_Monstre) {
            Matrice m = new Matrice(carte.getM());
            for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                    int num = 0;//correspond au numéro de la matrice à poser

                    //Pour toutes les rotations
                    int k = 0;
                    while (k < 4) {
                        //Récupération du score actuel
                        int decretA = score_Decret[saison][0];
                        int decretB = score_Decret[saison][1];
                        int or = score_Or.get(saison);
                        int gobelin = score_Gobelin.get(saison);

                        poserZoneUnique(m, "Monstre", i, j);

                        //recalcul des scores
                        scoreOr();
                        scoreGobelin();
                        scoreDecret();

                        if (score > score_Decret[saison][0] + score_Decret[saison][1] + score_Or.get(saison) - score_Gobelin.get(saison)) {
                            x = i;
                            y = j;
                            score = score_Decret[saison][0] + score_Decret[saison][1] + score_Or.get(saison) - score_Gobelin.get(saison);
                            numMatrice = num;
                            numRotate = k;
                        }

                        //Reset des scores et map
                        score_Decret[saison][0] = decretA;
                        score_Decret[saison][1] = decretB;
                        score_Or.set(saison, or);
                        score_Gobelin.set(saison, gobelin);
                        setMatrice_Joueur(ex_matrice_Joueur);

                        m.rotateCarte();
                        k++;
                    }

                    num++;


                }
            }
            m = new Matrice(carte.getM());
            int rotate = 0;
            while(rotate<numRotate){
                m.rotateCarte();
                rotate++;
            }
            if(!poserZoneUnique(m,"Monstre",x,y)){
                impossible(carte);
            }
            else {
                poserZoneUnique(m,"Monstre",x,y);
            }
        }


    }


    /*
    Méthode pour ajouter une zone au hasard si aucun emplacement n'est avantageux
     */
    public void noIdea(Carte_De_Jeu carte){

        Random pose = new Random();
        int nbTest = 0;
        if(carte instanceof Carte_Exploration_Unique){

            //Tant qu'il n'est pas possible de poser la zone à la place actuelle, tenter de la place autre part
            boolean possible = poserZoneUnique(((Carte_Exploration_Unique) carte).getListMatrice().get(0),((Carte_Exploration_Unique) carte).getType(),pose.nextInt(matrice_Joueur.getLigne()-1), pose.nextInt(matrice_Joueur.getColonne()-1));
            while (!possible && nbTest<100){
                possible = poserZoneUnique(((Carte_Exploration_Unique) carte).getListMatrice().get(0),((Carte_Exploration_Unique) carte).getType(),pose.nextInt(matrice_Joueur.getLigne()-1), pose.nextInt(matrice_Joueur.getColonne()-1));
                nbTest++;
            }
            if (nbTest>=100){
                impossible(carte);
            }

        }
        else if(carte instanceof Carte_Exploration_Choix){

            //Tant qu'il n'est pas possible de poser la zone à la place actuelle, tenter de la place autre part
            boolean possible = poserZoneUnique(carte.getM(), ((Carte_Exploration_Choix) carte).getListChoix().get(pose.nextInt(2)),pose.nextInt(matrice_Joueur.getLigne()-1), pose.nextInt(matrice_Joueur.getColonne()-1));
            while (!possible && nbTest<100) {
                possible = poserZoneUnique(carte.getM(), ((Carte_Exploration_Choix) carte).getListChoix().get(pose.nextInt(2)), pose.nextInt(matrice_Joueur.getLigne() - 1), pose.nextInt(matrice_Joueur.getColonne() - 1));
                nbTest++;
            }
            if (nbTest>=100){
                impossible(carte);
            }
        }

    }

    /*
    Méthode pour poser une zone où un endroit est impossible après trop de test en random (noIdea)
     */
    public void impossible(Carte_De_Jeu carte){


        if (carte instanceof Carte_Exploration_Unique) {//Si c'est une carte Unique, alors il y a les deux matrices de la carte à tester
            for (Matrice m0 : ((Carte_Exploration_Unique) Controller.getCarte()).getListMatrice()) {//Tester les deux matrices
                //1 -> recopier la matrice de base
                var ex = new Matrice(m0.getLigne(), m0.getColonne(), m0.getMatrice());
                Case[][] mat = new Case[m0.getLigne()][m0.getColonne()];
                for (int i = 0; i < m0.getLigne(); i++) {
                    for (int j = 0; j < m0.getColonne(); j++) {

                        mat[i][j] = new Case(m0.getMatrice()[i][j].getX(), m0.getMatrice()[i][j].getY(), m0.getMatrice()[i][j].getType(), m0.getMatrice()[i][j].getNbPoint());

                    }
                }
                ex.setMatrice(mat);
                //2 ->
                //On va lui dire test de poser la zone sur chaque case de ta map, si un moment ça te retourne true c'est que tu peux sinon c'est impossible
                for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                    for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                        int k = 0;
                        while (k < 4) {//Pour les quatres rotations
                            if (poserZoneUnique(ex, ((Carte_Exploration_Unique) carte).getType(), i, j)) {
                                return;
                            }
                            ex.rotateCarte();//Rotate la carte
                            k++;
                            setMatrice_Joueur(ex_matrice_Joueur);//Réinitialisation de la matrice du joueur
                        }


                    }
                }
                return;
            }
        } else if (carte instanceof Carte_Exploration_Choix){
            var m = carte.getM();
            Random random = new Random();
            var ex = new Matrice(m.getLigne(), m.getColonne(), m.getMatrice());
            Case[][] mat = new Case[m.getLigne()][m.getColonne()];
            for (int i = 0; i < m.getLigne(); i++) {
                for (int j = 0; j < m.getColonne(); j++) {

                    mat[i][j] = new Case(m.getMatrice()[i][j].getX(), m.getMatrice()[i][j].getY(), m.getMatrice()[i][j].getType(), m.getMatrice()[i][j].getNbPoint());

                }
            }
            ex.setMatrice(mat);

            //On va lui dire test de poser la zone sur chaque case de ta map, si un moment ça te retourne true c'est que tu peux sinon c'est impossible
            for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                    int k = 0;
                    while (k < 4) {//Pour les trois rotations
                        if (poserZoneUnique(ex, ((Carte_Exploration_Choix) carte).getListChoix().get(random.nextInt(2)), i, j)) {
                            setMatrice_Joueur(ex_matrice_Joueur);
                            return;
                        }
                        ex.rotateCarte();
                        k++;
                        setMatrice_Joueur(ex_matrice_Joueur);
                    }


                }
            }
        }
        else {
            var m = carte.getM();
            var ex = new Matrice(m.getLigne(), m.getColonne(), m.getMatrice());
            Case[][] mat = new Case[m.getLigne()][m.getColonne()];
            for (int i = 0; i < m.getLigne(); i++) {
                for (int j = 0; j < m.getColonne(); j++) {

                    mat[i][j] = new Case(m.getMatrice()[i][j].getX(), m.getMatrice()[i][j].getY(), m.getMatrice()[i][j].getType(), m.getMatrice()[i][j].getNbPoint());

                }
            }
            ex.setMatrice(mat);

            //On va lui dire test de poser la zone sur chaque case de ta map, si un moment ça te retourne true c'est que tu peux sinon c'est impossible
            for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                    int k = 0;
                    while (k < 4) {//Pour les trois rotations
                        if (poserZoneUnique(ex, "Monstre", i, j)) {
                            setMatrice_Joueur(ex_matrice_Joueur);
                            return;
                        }
                        ex.rotateCarte();
                        k++;
                        setMatrice_Joueur(ex_matrice_Joueur);
                    }


                }
            }
        }

    }

}
