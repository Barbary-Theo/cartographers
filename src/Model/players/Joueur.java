package Model.players;

import Controller.Controller;
import Model.Jeu.Jeu;
import Model.cartes.Carte_Exploration_Unique;
import Model.cartes.Carte_Monstre;
import Model.cartes.Carte_Ruine;
import Model.cartes.Saison;
import Model.maps.Case;
import Model.maps.Matrice;

import java.io.Serializable;
import java.util.*;

public class Joueur implements Serializable {
    protected String nom;
    protected String titre;
    protected ArrayList<Integer> score;
    protected int[][] score_Decret;
    protected ArrayList<Integer> score_Or;
    protected ArrayList<Integer> score_Gobelin;
    protected int score_fin;
    protected Matrice matrice_Joueur;
    protected Matrice ex_matrice_Joueur;//Map du joueur avant le dernier poser zone

    public Joueur(String nom, String titre) {
        super();
        this.nom = nom;
        this.titre = titre;
        this.matrice_Joueur = Matrice.initMap();
        this.ex_matrice_Joueur = matrice_Joueur;
        this.score = new ArrayList<>();
        this.score_Gobelin= new ArrayList<>();
        this.score_Or = new ArrayList<>();
        this.score_Decret = new int[4][2];
        for(int i=0 ; i<4 ; i++){
            for(int j=0 ; j<2 ; j++){
                score_Decret[i][j] = 0;
            }
        }
        score.add(0);score.add(0);score.add(0);score.add(0);//Ajout des scores de bases
        score_Gobelin.add(0);score_Gobelin.add(0);score_Gobelin.add(0);score_Gobelin.add(0);
        score_Or.add(0);score_Or.add(0);score_Or.add(0);score_Or.add(0);

    }


    /*
    Méthode pour check si les quatres côtés des cases montagnes sont remplis -> si oui ajouter un d'or
     */
    public void checkOrMontagne() {
        int numSaison = Jeu.getInstance().getCurrent_saison().getNumSaison();

        for (int i=0;i<this.matrice_Joueur.getLigne();i++) {
            for(int j=0;j<this.matrice_Joueur.getColonne();j++){
                //si c'est une case montagne et que ses côtés ne sont pas vide
                if (this.matrice_Joueur.getMatrice()[i][j].getType().equals("Montagne") && (!matrice_Joueur.getMatrice()[i][j].isCheck()) &&
                        !this.matrice_Joueur.getMatrice()[i + 1][j].getType().equals("Vide") && !this.matrice_Joueur.getMatrice()[i - 1][j].getType().equals("Vide")
                        && !this.matrice_Joueur.getMatrice()[i][j + 1].getType().equals("Vide") && !this.matrice_Joueur.getMatrice()[i][j - 1].getType().equals("Vide")){
                    matrice_Joueur.getMatrice()[i][j].setCheck(true);//Dire que cette montagne a déjà été check pour pas la recomptabiliser les saisons d'après
                    this.score_Or.set(numSaison,score_Or.get(numSaison)+1);//Ajouter 1 d'or au score or de la saison actuelle
                }

            }
        }

    }

    /*
    Permet de poser une zone ou non sur la map du joueur
    A partir de la position (l,c) passé aussi en paramètre
     */
    public boolean poserZoneUnique(Matrice carte, String type, int l,int c) {

        //Affectation de l'ancienne matrice à exMatrice avant le changement.
        this.setEx_Matrice_Joueur(new Matrice(matrice_Joueur));

        //Initialisation des variables
        Map<Integer,ArrayList<Integer>> pos = new HashMap<>();//Permetant d'ajouter les coordonnées de la carte où ce n'est pas vide ('bois',etc)
        int k=0;
        var res = new ArrayList<Integer>();
        boolean possible = true;
        boolean onRuine = false;

        if((l+3-carte.getLigne())>=0 && (c+3-carte.getColonne())>=0 && (l+ carte.getLigne())<=11 && (c+carte.getColonne())<=11){//si la zone ne dépassera pas la map à partir de l'endroit cliqué
            //Pour les lignes et les colonnes de la zone à placer
            for(int i=0 ; i<carte.getLigne() ; i++){
                for(int j=0 ; j<carte.getColonne() ; j++){
                    //Si la case sur la zone à placer n'est pas vide
                    if(!carte.getMatrice()[i][j].getType().equals("Vide")) {
                        res = new ArrayList<>();res.add(i);res.add(j);//ajouter la liste des coordonnées (get(0) => ligne, get(1)=>colonne)
                        pos.put(k,res);//Les ajouter au Map avec comme clé k
                    }
                    k++;
                }
            }

             if (!(Controller.getExCarte()  instanceof Carte_Ruine) || (Jeu.getInstance().getCurrent_carte()==Jeu.getInstance().getCarteImpossible()) ){//Si ce n'était pas une carte ruine avant ou que le joueur n'a pas de coup possible
                for (ArrayList<Integer> in : pos.values()) {//pour toutes les coordonnées où ce n'est pas vide sur la zone à placer
                    //Si sur la matrice du joueur au coordonné à placer plus les coodonnées de la liste n'est pas vide (déjà occupé)
                    if (!this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Vide") && !this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Temple")) {
                        possible = false;//Dire que ce n'est pas possible d'ajouter et sortir de la boucle
                        break;
                    }
                    if (this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Temple") && type.equals("Monstre")) {//Si c'est une carte Monstre et qu'on pose sur une ruine
                        possible = false;//Dire que ce n'est pas possible d'ajouter et sortir de la boucle
                        break;
                    }

                }
            }
            //Si la carte précédente était une carte ruine alors on doit poser cette zone sur une case ruine
            else {
                for (ArrayList<Integer> in : pos.values()) {//pour toutes les coordonnées où ce n'est pas vide sur la zone à placer
                    //Si sur la matrice du joueur au coordonné à placer plus les coodonnées de la liste n'est pas vide (déjà occupé)
                    if (!this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Vide") && !this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Temple")) {
                        possible = false;//Dire que ce n'est pas possible d'ajouter et sortir de la boucle
                        break;
                    }
                    if (this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Temple") && type.equals("Monstre")) {//Si c'est une carte Monstre et qu'on pose sur une ruine
                        possible = false;//Dire que ce n'est pas possible d'ajouter et sortir de la boucle
                        break;
                    }
                    if(this.getMatrice_Joueur().getMatrice()[l + (in.get(0))][c + (in.get(1))].getType().equals("Temple")){
                        onRuine = true;
                    }

                }
                if(!onRuine) return false;//Si aucune case n'est sur une ruine return false directement

            }


            //Si c'est possible
            if (possible){

                for (ArrayList<Integer> in : pos.values()){//pour toutes les coordonnées où ce n'est pas vide sur la zone à placer
                    //Changer le type de la case au coordonnée l+position de la liste et c + position de la liste sur la map du joueur, par le type des coordonnées de la zone
                    this.getMatrice_Joueur().changerCase(l+(in.get(0)),c+(in.get(1)),carte.getMatrice()[in.get(0)][in.get(1)].getType());
                }

                return true;


            }
            else {
                return false;
            }
        }
        else {
            return false;
        }



    }

    /*
    Méthode pour vérifier si le joueur peu encore poser la zone sur sa map (n'importe où)
     */
    public boolean isPossible(Matrice m, String type){


        if (Controller.getCarte() instanceof Carte_Exploration_Unique) {//Si c'est une carte Unique, alors il y a les deux matrices de la carte à tester
            for (Matrice m0 : ((Carte_Exploration_Unique) Controller.getCarte()).getListMatrice()) {//Tester les deux matrices
                //1 -> recopier la matrice de base
                var ex = new Matrice(m0.getLigne(), m0.getColonne(), m0.getMatrice());
                Case[][] mat = new Case[m0.getLigne()][m0.getColonne()];
                for (int i = 0; i < m0.getLigne(); i++) {
                    for (int j = 0; j < m0.getColonne(); j++) {

                        mat[i][j] = new Case(m0.getMatrice()[i][j].getX(), m0.getMatrice()[i][j].getY(), m0.getMatrice()[i][j].getType(), m0.getMatrice()[i][j].getNbPoint());

                    }
                }
                ex.setMatrice(mat);
                //2 ->
                //On va lui dire test de poser la zone sur chaque case de ta map, si un moment ça te retourne true c'est que tu peux sinon c'est impossible
                for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                    for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                        int k = 0;
                        while (k < 4) {//Pour les quatres rotations
                            if (poserZoneUnique(ex, type, i, j)) {
                                setMatrice_Joueur(ex_matrice_Joueur);//Réinitialisation de la matrice du joueur
                                return true;
                            }
                            ex.rotateCarte();//Rotate la carte
                            k++;
                            setMatrice_Joueur(ex_matrice_Joueur);//Réinitialisation de la matrice du joueur
                        }


                    }
                }
                return false;
            }
        } else {

            var ex = new Matrice(m.getLigne(), m.getColonne(), m.getMatrice());
            Case[][] mat = new Case[m.getLigne()][m.getColonne()];
            for (int i = 0; i < m.getLigne(); i++) {
                for (int j = 0; j < m.getColonne(); j++) {

                    mat[i][j] = new Case(m.getMatrice()[i][j].getX(), m.getMatrice()[i][j].getY(), m.getMatrice()[i][j].getType(), m.getMatrice()[i][j].getNbPoint());

                }
            }
            ex.setMatrice(mat);

            //On va lui dire test de poser la zone sur chaque case de ta map, si un moment ça te retourne true c'est que tu peux sinon c'est impossible
            for (int i = 0; i < matrice_Joueur.getLigne(); i++) {
                for (int j = 0; j < matrice_Joueur.getColonne(); j++) {

                    int k = 0;
                    while (k < 4) {//Pour les trois rotations
                        if (poserZoneUnique(ex, type, i, j)) {
                            setMatrice_Joueur(ex_matrice_Joueur);
                            return true;
                        }
                        ex.rotateCarte();
                        k++;
                        setMatrice_Joueur(ex_matrice_Joueur);
                    }


                }
            }
            return false;
        }
        return false;

    }


    public void poserZoneSolo(Carte_Monstre carte){

        var zone = carte.getM();
        var num = carte.getNum();
        boolean done = false;

        if(num==1){

            for(int i=0 ; i<matrice_Joueur.getLigne() ; i++){
                for(int j=0 ; j<matrice_Joueur.getColonne() ; j++){

                    if(poserZoneUnique(zone,"Monstre",j,i)){
                        done = true;
                        break;
                    }
                }
                if(done) break;
            }

        }
        else if(num==2){

            for(int j=matrice_Joueur.getColonne() ; j>0 ; j--){
                for (int i=0 ; i<matrice_Joueur.getLigne() ; i++) {

                    if(poserZoneUnique(zone,"Mosntre",i,j)){
                        done = true;
                        break;
                    }
                }
                if(done) break;
            }

        }
        else if(num==3){

            for(int j=0 ; j<matrice_Joueur.getColonne() ; j++){
                for (int i=matrice_Joueur.getLigne() ; i>0 ; i--) {

                    if(poserZoneUnique(zone,"Mosntre",i,j)){
                        done = true;
                        break;
                    }
                }
                if(done) break;
            }

        }
        else {

            for(int j=matrice_Joueur.getColonne() ; j>0 ; j--){
                for (int i=matrice_Joueur.getLigne() ; i>0 ; i--) {

                    if(poserZoneUnique(zone,"Mosntre",i,j)){
                        done = true;
                        break;
                    }
                }
                if(done) break;
            }

        }

    }



    public int calculScoreGobelin(){

        int res = 0;
        var map = this.matrice_Joueur;

        for (int i = 0; i < map.getLigne(); i++) {
            for (int j = 0; j < map.getColonne(); j++) {

                if (map.getMatrice()[i][j].getType().equals("Monstre") && !map.getMatrice()[i][j].isCheck()){
                    map.getMatrice()[i][j].setCheck(true);
                    res += gobelin(0, map, i, j);
                }

            }
        }

        for (int i = 0; i < map.getLigne(); i++) {
            for (int j = 0; j < map.getColonne(); j++) {
                if(!map.getMatrice()[i][j].getType().equals("Montagne")) map.getMatrice()[i][j].setCheck(false);
            }
        }

        return res;
    }

    /*
    Methode en supplement de la verify du score goblin
     */
    public static int gobelin(int res, Matrice map, int x, int y){


        if (x != 0 && x != map.getLigne() - 1 && y != 0 && y != map.getColonne() - 1) {
            if((map.getMatrice()[x + 1][y].getType().equals("Vide") || map.getMatrice()[x + 1][y].getType().equals("Temple")) && !map.getMatrice()[x+1][y].isCheck()) {map.getMatrice()[x+1][y].setCheck(true);res ++; }
            if((map.getMatrice()[x][y + 1].getType().equals("Vide") || map.getMatrice()[x][y + 1].getType().equals("Temple")) && !map.getMatrice()[x][y+1].isCheck()) {map.getMatrice()[x][y+1].setCheck(true);res ++; }
            if((map.getMatrice()[x - 1][y].getType().equals("Vide") || map.getMatrice()[x - 1][y].getType().equals("Temple")) && !map.getMatrice()[x-1][y].isCheck()) {map.getMatrice()[x-1][y].setCheck(true);res ++; }
            if((map.getMatrice()[x][y - 1].getType().equals("Vide") || map.getMatrice()[x][y - 1].getType().equals("Temple")) && !map.getMatrice()[x][y-1].isCheck()) {map.getMatrice()[x][y-1].setCheck(true);res ++; }

        }
        else if (x == 0 && y == 0) {
            if((map.getMatrice()[x + 1][y].getType().equals("Vide") || map.getMatrice()[x + 1][y].getType().equals("Temple"))&& !map.getMatrice()[x+1][y].isCheck()) {map.getMatrice()[x+1][y].setCheck(true);res ++; }
            if((map.getMatrice()[x][y + 1].getType().equals("Vide") || map.getMatrice()[x][y + 1].getType().equals("Temple"))&& !map.getMatrice()[x][y+1].isCheck()) {map.getMatrice()[x][y+1].setCheck(true);res ++; }
        }
        else if (x == map.getLigne() - 1 && y == map.getColonne() - 1) {
            if((map.getMatrice()[x - 1][y].getType().equals("Vide") || map.getMatrice()[x - 1][y].getType().equals("Temple")) && !map.getMatrice()[x-1][y].isCheck()) {map.getMatrice()[x-1][y].setCheck(true);res ++; }
            if((map.getMatrice()[x][y - 1].getType().equals("Vide") || map.getMatrice()[x][y - 1].getType().equals("Temple")) && !map.getMatrice()[x][y-1].isCheck()) {map.getMatrice()[x][y-1].setCheck(true);res++; }

        }
        else if (x == 0 && y == map.getColonne() - 1) {
            if((map.getMatrice()[x + 1][y].getType().equals("Vide") || map.getMatrice()[x + 1][y].getType().equals("Temple")) && !map.getMatrice()[x+1][y].isCheck() ){ map.getMatrice()[x+1][y].setCheck(true);res++; }
            if((map.getMatrice()[x][y - 1].getType().equals("Vide") || map.getMatrice()[x][y - 1].getType().equals("Temple")) && !map.getMatrice()[x][y-1].isCheck()) {map.getMatrice()[x][y-1].setCheck(true);res++; }

        }
        else if (y == 0 && x == map.getLigne() - 1) {
            if((map.getMatrice()[x][y + 1].getType().equals("Vide") || map.getMatrice()[x][y + 1].getType().equals("Temple")) && !map.getMatrice()[x][y+1].isCheck()){ map.getMatrice()[x][y+1].setCheck(true);res ++; }
            if((map.getMatrice()[x - 1][y].getType().equals("Vide") || map.getMatrice()[x - 1][y].getType().equals("Temple")) && !map.getMatrice()[x-1][y].isCheck()) {map.getMatrice()[x-1][y].setCheck(true);res ++; }

        }
        else if (x == 0) {
            if((map.getMatrice()[x + 1][y].getType().equals("Vide") || map.getMatrice()[x + 1][y].getType().equals("Temple"))&& !map.getMatrice()[x+1][y].isCheck()) {map.getMatrice()[x+1][y].setCheck(true);res++ ; }
            if((map.getMatrice()[x][y + 1].getType().equals("Vide") || map.getMatrice()[x][y + 1].getType().equals("Temple"))&& !map.getMatrice()[x][y+1].isCheck()) {map.getMatrice()[x][y+1].setCheck(true);res++  ; }
            if((map.getMatrice()[x][y - 1].getType().equals("Vide") || map.getMatrice()[x][y - 1].getType().equals("Temple")) && !map.getMatrice()[x][y-1].isCheck()) {map.getMatrice()[x][y-1].setCheck(true);res ++ ; }

        }
        else if (y == 0) {
            if((map.getMatrice()[x + 1][y].getType().equals("Vide") || map.getMatrice()[x + 1][y].getType().equals("Temple")) && !map.getMatrice()[x+1][y].isCheck()) {map.getMatrice()[x+1][y].setCheck(true);res ++; }
            if ((map.getMatrice()[x][y + 1].getType().equals("Vide") || map.getMatrice()[x][y + 1].getType().equals("Temple"))&& !map.getMatrice()[x][y+1].isCheck()){ map.getMatrice()[x][y+1].setCheck(true);res ++ ; }
            if((map.getMatrice()[x - 1][y].getType().equals("Vide") || map.getMatrice()[x - 1][y].getType().equals("Temple")) && !map.getMatrice()[x-1][y].isCheck()) {map.getMatrice()[x-1][y].setCheck(true);res ++ ; }

        }
        else if (y == map.getColonne() - 1) {
            if((map.getMatrice()[x + 1][y].getType().equals("Vide") || map.getMatrice()[x + 1][y].getType().equals("Temple")) && !map.getMatrice()[x+1][y].isCheck()){ map.getMatrice()[x+1][y].setCheck(true);res ++ ; }
            if((map.getMatrice()[x - 1][y].getType().equals("Vide") || map.getMatrice()[x - 1][y].getType().equals("Temple")) && !map.getMatrice()[x-1][y].isCheck()) { map.getMatrice()[x-1][y].setCheck(true);res ++  ;}
            if((map.getMatrice()[x][y - 1].getType().equals("Vide") || map.getMatrice()[x][y - 1].getType().equals("Temple")) && !map.getMatrice()[x][y-1].isCheck()) {map.getMatrice()[x][y-1].setCheck(true);res ++ ; }

        }
        else if (x == map.getLigne() - 1) {
            if( (map.getMatrice()[x][y + 1].getType().equals("Vide") || map.getMatrice()[x][y + 1].getType().equals("Temple"))&& !map.getMatrice()[x][y+1].isCheck()) { map.getMatrice()[x][y+1].setCheck(true);res ++; }
            if((map.getMatrice()[x - 1][y].getType().equals("Vide") || map.getMatrice()[x - 1][y].getType().equals("Temple")) && !map.getMatrice()[x-1][y].isCheck()) { map.getMatrice()[x-1][y].setCheck(true);res ++ ;}
            if((map.getMatrice()[x][y - 1].getType().equals("Vide") || map.getMatrice()[x][y - 1].getType().equals("Temple")) && !map.getMatrice()[x][y-1].isCheck()) {map.getMatrice()[x][y-1].setCheck(true);res ++ ; }

        }
        return res;
    }

    public String getNom() {
        return nom;
    }

    public String getTitre() {
        return titre;
    }

    public ArrayList<Integer> getScore() {
        return score;
    }

    public ArrayList<Integer> getScore_Or() { return score_Or;  }

    public ArrayList<Integer> getScore_Gobelin() {
        return score_Gobelin;
    }

    public int[][] getScore_Decret() {
        return score_Decret;
    }

    public Matrice getMatrice_Joueur() {
        return matrice_Joueur;
    }

    public Matrice getEx_Matrice_Joueur() {
        return ex_matrice_Joueur;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setMatrice_Joueur(Matrice matrice_Joueur) {
        this.matrice_Joueur = matrice_Joueur;
    }

    public void setEx_Matrice_Joueur(Matrice mtrice_Joueur) { this.ex_matrice_Joueur = mtrice_Joueur;}

    public void addPiece(){
        int numSaison = Jeu.getInstance().getCurrent_saison().getNumSaison();
        this.score_Or.set(numSaison,this.score_Or.get(numSaison)+1);//Ajouter une piece d'or à la saison actuelle
    }

    /*
    Methode pour calculer le score final de la saison
     */
    public void scoreOr(){
        int saison = Jeu.getInstance().getCurrent_saison().getNumSaison();
        ArrayList<Integer> nouveau = new ArrayList<>(score_Or);
        if(saison!=0) nouveau.set(saison,score_Or.get(saison)+score_Or.get(saison-1));
        else nouveau.set(saison,score_Or.get(saison));
        this.score_Or = nouveau;
    }

    public void scoreGobelin(){
        int valeur = calculScoreGobelin();
        int saison = Jeu.getInstance().getCurrent_saison().getNumSaison();
        this.score_Gobelin.set(saison,valeur);
    }

    public void scoreDecret(){
        Saison saison = Jeu.getInstance().getCurrent_saison();
        int numSaison = saison.getNumSaison();

        int scoreDecret1 = saison.getDecrets().get(0).Verif_Point(matrice_Joueur);
        int scoreDecret2 = saison.getDecrets().get(1).Verif_Point(matrice_Joueur);

        this.score_Decret[numSaison][0] = scoreDecret1;
        this.score_Decret[numSaison][1] = scoreDecret2;

    }


    public void scoreFinalSaison(){
        int saison = Jeu.getInstance().getCurrent_saison().getNumSaison();
        score.set(saison,(score_Decret[saison][0]+score_Decret[saison][1]+score_Or.get(saison)-score_Gobelin.get(saison)));

        int scorefinal = 0;
        for(int s : score){
            scorefinal += s;
        }
        this.score_fin = scorefinal;
    }

    public int getScore_fin(){
        return this.score_fin;
    }

}