package Model.cartes;

import Model.maps.Matrice;

import java.io.File;

public abstract class Carte_Exploration extends Carte_De_Jeu {

    protected Matrice m;

    public Carte_Exploration(File image, int duree) {
        super(image);
        this.duree = duree;
    }

    public int getDuree() {
        return duree;
    }

    @Override
    public void setM(Matrice m) {
        this.m = m;
    }

    public Matrice getM() {
        return m;
    }
}