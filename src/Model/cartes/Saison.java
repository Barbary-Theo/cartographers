package Model.cartes;

import java.io.File;
import java.util.ArrayList;

public class Saison extends Carte {
    protected int numSaison;
    protected ArrayList<Decret> decrets;
    protected int dureeSaison;

    public Saison(File image, int dureeSaison, int numSaison) {
        super(image);
        this.numSaison = numSaison;
        this.decrets = new ArrayList<>();
        this.dureeSaison = dureeSaison;
    }

    public ArrayList<Decret> getDecrets() {
        return decrets;
    }

    public int getDureeSaison() {
        return dureeSaison;
    }

    public int getNumSaison() {
        return numSaison;
    }

}
