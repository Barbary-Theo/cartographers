package Model.cartes;

import java.io.File;
import java.io.Serializable;

public abstract class Carte implements Serializable {
    protected File image;

    public Carte(File image) {
        this.image = image;
    }

    public File getImage(){
        return this.image;
    }
}