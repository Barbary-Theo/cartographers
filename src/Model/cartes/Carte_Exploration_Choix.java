package Model.cartes;


import java.io.File;
import java.util.ArrayList;

public class Carte_Exploration_Choix extends Carte_Exploration {
    private ArrayList<String> ListChoix;

    public Carte_Exploration_Choix(File image, int duree){
        super(image,duree);
        this.ListChoix = new ArrayList<>();
    }

    public ArrayList<String> getListChoix() {
        return ListChoix;
    }

}