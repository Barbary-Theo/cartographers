package Model.cartes;

import java.io.File;

public class Carte_Ruine extends Carte_De_Jeu {
    public Carte_Ruine(File image) {
        super(image);
        super.duree=0;
    }

    @Override
    public int getDuree() {
        return 0;
    }

}