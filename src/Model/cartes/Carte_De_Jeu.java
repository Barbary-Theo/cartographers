package Model.cartes;

import Model.maps.Matrice;

import java.io.File;

public abstract class Carte_De_Jeu extends Carte {
    protected Matrice m;
    protected int duree;

    public Carte_De_Jeu(File image) {
        super(image);
        this.m = null;
    }

    public void setM(Matrice m) {
        this.m = m;
    }

    public Matrice getM(){
        return this.m;
    }

    public int getDuree(){
        return this.duree;
    }

}