package Model.cartes;

import Model.maps.Matrice;

import java.io.File;
import java.util.ArrayList;

public class Carte_Exploration_Unique extends Carte_Exploration {
    protected ArrayList<Matrice> ListMatrice;
    protected String type;


    public Carte_Exploration_Unique(File image, int duree, String t) {
        super(image,duree);
        type=t;
        this.ListMatrice = new ArrayList<>();
    }

    public ArrayList<Matrice> getListMatrice(){
        return ListMatrice;
    }


    public void setListMatrice(Matrice m1, Matrice m2) {
         this.ListMatrice.add(m1);this.ListMatrice.add(m2);

    }

    public String getType(){ return this.type; }
}
