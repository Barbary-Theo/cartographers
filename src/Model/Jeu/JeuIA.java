package Model.Jeu;

import Controller.Controller;
import Model.Music.FinMusic;
import Model.Music.MonstreMusic;
import Model.Music.SaisonMusic;
import Model.Music.TempleMusic;
import Model.cartes.Carte_Monstre;
import Model.cartes.Carte_Ruine;
import Model.players.IA;
import Model.players.Joueur;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class JeuIA extends Jeu {


    public JeuIA(ArrayList<Joueur> player) {

        super(player);
    }

    public static Jeu getInstance(int nb, String nom, String titre){
        var player = new ArrayList<Joueur>();
        player.add(new Joueur(nom,titre));

        var tab = createIA(nb);

        player.addAll(tab);

        if (jeu == null) jeu = new JeuIA(player);
        return jeu;
    }

    public static ArrayList<Joueur> createIA(int nb){
        String[] noms = {"Damien","Raphaël","Roman","Théo","Younes","Léo","Louis","Justin","Sacha","Bidjorgen","PataThor","Edward","Boubakar","Liam","Adil", "Camille","Alexandre"};
        String[] titres = {"Le Grand","Le Vaillant","Le Loubard","Le croco","Le fin","Le marquis","Le baron","L'intrépide","L'incroyable","Le fou","L'apprentie","Le costaud","L'Ambicieux","Le voleur","Le malin", "Le généreux","Le cogneur"};
        Random rand = new Random();

        var liste = new ArrayList<Joueur>();


        for (int i=0 ; i<nb ; i++){
            liste.add(new IA(noms[rand.nextInt(noms.length-1)],titres[rand.nextInt(noms.length-1)]));
        }

        return liste;
    }

    /*
    si c'est une carte Mosntre, échanger la map entre les joueurs
     */
    public void rotationMonstre(Carte_Monstre c){

        ArrayList<Joueur> nouveau = new ArrayList<>(joueur);
        int j = this.joueur.size()-1;
        if(!c.isType_rotation()){//Si c'est vers la droite
            var dev = this.joueur.get(0).getMatrice_Joueur();
            for(int i=0 ; i<this.joueur.size()-1 ; i++){//Décaler toutes les maps des joueurs vers la droite
                nouveau.get(i).setMatrice_Joueur(this.joueur.get(i+1).getMatrice_Joueur());
            }
            nouveau.get(this.joueur.size()-1).setMatrice_Joueur(dev);
        }
        else {//Si c'est vers la gauche
            var dev = this.joueur.get(this.joueur.size()-1).getMatrice_Joueur();
            for(int i=0 ; i<this.joueur.size()-1 ; i++){
                nouveau.get(j-i).setMatrice_Joueur(this.joueur.get(j-i-1).getMatrice_Joueur());
            }
            nouveau.get(0).setMatrice_Joueur(dev);
        }

        this.joueur = nouveau;

    }

    public void resetRotationMonstre(Carte_Monstre c){
        ArrayList<Joueur> nouveau = new ArrayList<>(joueur);
        int j = this.joueur.size()-1;
        if(c.isType_rotation()){//Si c'est vers la droite
            var dev = this.joueur.get(0).getMatrice_Joueur();
            for(int i=0 ; i<this.joueur.size()-1 ; i++){//Décaler vers la gauche
                nouveau.get(i).setMatrice_Joueur(this.joueur.get(i+1).getMatrice_Joueur());
            }
            nouveau.get(this.joueur.size()-1).setMatrice_Joueur(dev);
        }
        else {//Si c'est vers la gauche
            var dev = this.joueur.get(this.joueur.size()-1).getMatrice_Joueur();
            for(int i=0 ; i<this.joueur.size()-1 ; i++){//Décaler vers la droite
                nouveau.get(j-i).setMatrice_Joueur(this.joueur.get(j-i-1).getMatrice_Joueur());
            }
            nouveau.get(0).setMatrice_Joueur(dev);
        }

        this.joueur = nouveau;
    }

    public void EndTurn() {
        Random r = new Random();

        for(int i=0 ; i<joueur.size() ; i++){
            if(current_joueur.equals(joueur.get(i))){//Si le joueur de la liste est égale au current joueur


                current_joueur.checkOrMontagne();//Calcul des pièces d'or avec la montagne
                current_joueur.scoreGobelin();
                current_joueur.scoreDecret();

                if(!current_carte.equals(carteBeforeImpossible)){//Si le joueur ne pouvais pas placer ça carte
                    current_carte = carteBeforeImpossible;//Alors la carte à jouer sera de nouveau la carte de base pour les joueurs d'après
                }


                if(i==joueur.size()-1){//Si c'était le dernier Joueur de la liste
                    current_joueur = joueur.get(0);//Le prochain joueur est le premier joueur

                    nbTour += carteBeforeImpossible.getDuree();
                    if(current_saison.getDureeSaison()<=nbTour){//Si c'est la fin de la saison

                        for(int j=0 ; j<saison.size() ; j++){

                            if(current_saison.equals(saison.get(j))){//Si la saison de la liste est égale à la current saison

                                for(Joueur player : joueur){//Pour tout les joueurs
                                    //Partie calcul du score
                                    if(current_saison.getNumSaison()!=0){//Si c'est pas la première saison
                                        player.scoreOr();//Calcul du score d'or total de la saison
                                    }
                                    player.scoreFinalSaison();

                                }

                                if(j==saison.size()-1){//Si c'est l'hiver

                                    ArrayList<Integer> score_total = new ArrayList<>();

                                    for(Joueur player : joueur){
                                        score_total.add(player.getScore_fin());
                                    }

                                    tri_insertion(score_total,joueur);
                                    fin = true;

                                    Thread music = new FinMusic();
                                    music.start();

                                    Controller.endGame();
                                }
                                else {
                                    current_saison = saison.get(j+1);//Sinon passer à la prochaine saison
                                    ajouterMonstre();
                                }
                                break;
                            }
                        }
                        Thread music = new SaisonMusic();
                        music.start();
                        if(fin) break;
                        nbTour = 0;
                    }

                    if(current_carte instanceof Carte_Monstre){//Si c'était une carte monstre remettre la matrice à chacun
                        resetRotationMonstre((Carte_Monstre) current_carte);
                    }

                    ex_carte = current_carte;//Affectation de l'ancienne carte
                    int indice = r.nextInt(cartesdeJeu.size()-1);//Changer la carte de jeu
                    current_carte = cartesdeJeu.get(indice);
                    carteBeforeImpossible = current_carte;
                    cartesDejaJouees.add(cartesdeJeu.get(indice));
                    cartesdeJeu.remove(indice);

                    if(current_carte instanceof Carte_Monstre){//Si c'est une carte monstre rotate les maps de chacun
                        rotationMonstre((Carte_Monstre) current_carte);
                        Thread music = new MonstreMusic();
                        music.start();
                    }
                    if(current_carte instanceof Carte_Ruine){//Si c'est une carte ruine mettre la musique du temple
                        Thread music = new TempleMusic();
                        music.start();
                    }
                    if(cartesdeJeu.size()==1){//Si il n'y a plus de carte de jeu après celle la
                        cartesdeJeu = cartesDejaJouees;//Les cartes à jouer sont celles déjà jouées
                        melanger();//Mais mélangées
                        cartesDejaJouees = new ArrayList<>();//Réinitialisation des cartes déjà jouées
                    }

                }
                else{
                    current_joueur = joueur.get(i + 1);//Sinon passer au joueur suivant de la liste
                }

                if(current_joueur instanceof IA){
                    ((IA) current_joueur).poserZoneIA(current_carte);
                }

                break;
            }
        }
    }



    /*
    Méthode pour trier les joueurs en fonction de leur score
    (Elle servira pour la fin de la partie, avant l'affichage des résultats)
     */
    public void tri_insertion(ArrayList<Integer> tab , ArrayList<Joueur> autre){

        int k, j;
        Joueur k2;
        for (int i=1 ; i<tab.size() ; i++) {
            k = tab.get(i);
            k2 = autre.get(i);
            j = i - 1;
            while (j >= 0 && k <tab.get(j)) {
                tab.set(j+1,tab.get(j));
                autre.set(j+1, autre.get(j));
                j -= 1;
            }
            tab.set(j+1,k);
            autre.set(j+1,k2);
        }
        Collections.reverse(autre);
        this.joueur = autre;

    }

}
