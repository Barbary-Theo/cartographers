package Model.Jeu;

import Controller.Controller;
import Model.Music.FinMusic;
import Model.Music.MonstreMusic;
import Model.Music.SaisonMusic;
import Model.Music.TempleMusic;
import Model.cartes.Carte_Monstre;
import Model.cartes.Carte_Ruine;
import Model.players.Joueur;

import java.util.ArrayList;
import java.util.Random;

public class JeuSolo extends Jeu {

    public JeuSolo(ArrayList<Joueur> player) {
        super(player);

    }

    //Singleton
    public static Jeu getInstance(ArrayList<String> noms){
        var res = new ArrayList<Joueur>(); res.add(new Joueur(noms.get(0), ""));
        if (jeu == null) jeu = new JeuSolo(res);
        return jeu;
    }

    //Méthode pour changer le joueur actuelle après avoir terminé le tour d'un des joueurs
    public void EndTurn(){
        Random r = new Random();


        current_joueur.checkOrMontagne();//Calcul des pièces d'or avec la montagne
        current_joueur.scoreGobelin();
        current_joueur.scoreDecret();

        if(!current_carte.equals(carteBeforeImpossible)){//Si le joueur ne pouvais pas placer ça carte
            current_carte = carteBeforeImpossible;//Alors la carte à jouer sera de nouveau la carte de base pour les joueurs d'après
        }


        nbTour += carteBeforeImpossible.getDuree();
        if(current_saison.getDureeSaison()<=nbTour){//Si c'est la fin de la saison

            for(int j=0 ; j<saison.size() ; j++){

                if(current_saison.equals(saison.get(j))){//Si la saison de la liste est égale à la current saison


                    //Partie calcul du score
                    if(current_saison.getNumSaison()!=0){//Si c'est pas la première saison
                        current_joueur.scoreOr();//Calcul du score d'or total de la saison
                    }
                    current_joueur.scoreFinalSaison();



                    if(j==saison.size()-1){//Si c'est l'hiver


                        fin = true;
                        current_joueur.setTitre(Resultat());
                        Thread music = new FinMusic();
                        music.start();

                        Controller.endGame();
                    }
                    else {
                        current_saison = saison.get(j+1);//Sinon passer à la prochaine saison
                        ajouterMonstre();
                    }
                    break;
                }
            }
            Thread music = new SaisonMusic();
            music.start();
            nbTour = 0;
        }


        ex_carte = current_carte;//Affectation de l'ancienne carte
        int indice = r.nextInt(cartesdeJeu.size()-1);//Changer la carte de jeu
        current_carte = cartesdeJeu.get(indice);
        carteBeforeImpossible = current_carte;
        cartesDejaJouees.add(cartesdeJeu.get(indice));
        cartesdeJeu.remove(indice);

        if(current_carte instanceof Carte_Monstre){//Si c'est une carte monstre rotate les maps de chacun
            current_joueur.poserZoneSolo((Carte_Monstre) current_carte);
            Thread music = new MonstreMusic();
            music.start();
        }
        if(current_carte instanceof Carte_Ruine){//Si c'est une carte ruine mettre la musique du temple
            Thread music = new TempleMusic();
            music.start();
        }
        if(cartesdeJeu.size()==1){//Si il n'y a plus de carte de jeu après celle la
            cartesdeJeu = cartesDejaJouees;//Les cartes à jouer sont celles déjà jouées
            melanger();//Mais mélangées
            cartesDejaJouees = new ArrayList<>();//Réinitialisation des cartes déjà jouées
        }

    }

    //Pour connaitre le résultat final
    public String Resultat(){

        int score = current_joueur.getScore_fin();

        if(score <= (-30) ){
            return "Buveur d'Encre Inconscient";
        }
        else if(score <= (-20) ){
            return "Mauvais Gribouilleur";
        }
        else if(score <= (-10)){
            return "Assistant Inapte";
        }
        else if(score <= (-5)){
            return "Conseiller Amateur";
        }
        else if(score <= 10){
            return "Apprenti Géomètre";
        }
        else if(score <= 20){
            return "Topographe Voyageur";
        }
        else if(score <= 30){
            return "Maître des Cartes";
        }
        else {
            return "Cartographe Légendaire";
        }

    }

}
