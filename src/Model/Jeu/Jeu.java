package Model.Jeu;

import Model.cartes.*;
import Model.maps.Matrice;
import Model.players.Joueur;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Jeu implements Serializable {

    protected static Jeu jeu;//Pour le singleton
    protected static boolean fin = false;
    protected ArrayList<Joueur> joueur = new ArrayList<>();
    public ArrayList<Carte_De_Jeu> cartesdeJeu = new ArrayList<>();
    protected ArrayList<Carte_De_Jeu> cartesDejaJouees = new ArrayList<>();
    protected ArrayList<Saison> saison = new ArrayList<>();
    protected ArrayList<Carte_Monstre> listMonstre = new ArrayList<>();
    protected Saison current_saison;
    protected Carte_De_Jeu current_carte;
    protected Carte_De_Jeu carteImpossible;// elle représentera la carte à jouer en cas de coup impossible
    protected Carte_De_Jeu carteBeforeImpossible;//Carte avant le remplacement par la carte en cas de coup impossible
    protected Carte_De_Jeu ex_carte;//permettra de savoir si l'ancienne carte était une carte ruine, et donc faire le traitement adéquat
    protected Joueur current_joueur;//récupérer le joueur actuellement entrain de faire son tour
    protected int nbTour=0;//La durée de la saison actuelle

    protected Jeu(ArrayList<Joueur> players){

        start();//Récupération de toutes les cartes
        ajouterMonstre();//Ajout d'une carte monste au hasard

        joueur.addAll(players);
        current_joueur = joueur.get(0);//Le premier joueur à jouer est le premier joueur de la liste

        Random r=new Random();

        int indice = r.nextInt(cartesdeJeu.size()-1);
        current_carte = cartesdeJeu.get(indice);//Initialisation de la carte à jouer
        carteBeforeImpossible = current_carte;
        cartesdeJeu.remove(indice);//Suppresion dans le tas de carte pour ne pas la repiocher plus tard dans la partie
        ex_carte = current_carte;//Au départ dire que l'ancienne carte est égale à la première


        var List_decret = Decret();


        indice = r.nextInt(List_decret.size()-1);
        Decret a = (Decret) List_decret.get(indice);
        List_decret.remove(indice);
        indice = r.nextInt(List_decret.size()-1);
        Decret b = (Decret) List_decret.get(indice);
        List_decret.remove(indice);
        indice = r.nextInt(List_decret.size()-1);
        Decret c = (Decret) List_decret.get(indice);
        List_decret.remove(indice);
        indice = r.nextInt(List_decret.size()-1);
        Decret d = (Decret) List_decret.get(indice);
        List_decret.remove(indice);
        saison.get(0).getDecrets().add(a);
        saison.get(0).getDecrets().add(b);
        saison.get(1).getDecrets().add(b);
        saison.get(1).getDecrets().add(c);
        saison.get(2).getDecrets().add(c);
        saison.get(2).getDecrets().add(d);
        saison.get(3).getDecrets().add(d);
        saison.get(3).getDecrets().add(a);
        current_saison = saison.get(0);

    }


    //principe du singleton, qu'un seul jeu dans une partie
    public static Jeu getInstance(){
        var j = new ArrayList<Joueur>();
        j.add(new Joueur("oui","non"));

        if (jeu == null) jeu = new Jeu(j);
        return jeu;
    }

    /*
    Initalisation de la partie noramlement
    Ajout des cartes de jeu
    Ajout des cartes monstre
     */
    public void start(){

        this.cartesdeJeu.addAll(Explo_unique());
        this.cartesdeJeu.addAll(Explo_Choix());
        this.listMonstre.addAll(Monstre());
        this.cartesdeJeu.addAll(Ruine());
        this.saison = Saisons();

    }

    public ArrayList<Carte_De_Jeu> Explo_unique(){
        String chemin="Cartes\\Exploration\\Uniques";  //Chemin du dossier des cartes
        File p=new File(chemin);
        File[] dossier=p.listFiles(); //Liste contenant tous les dossiers du fichier dans le chemin de la variable chemin
        ArrayList<File> ls=new ArrayList<>();
        ArrayList<Carte_De_Jeu> lc=new ArrayList<>();

        assert dossier != null;
        Collections.addAll(ls, dossier);

        ls=getOnlyUse(ls);

        Carte_Exploration_Unique c;
        for(File i : ls) {
            if (i.getName().charAt(2) == 'E'){ //On teste si la premiere lettre est un E
                c=new Carte_Exploration_Unique(i, 1, "Eau");
                Matrice m1=new Matrice(3,1);
                m1.changerCase(0,0,"Eau");
                m1.changerCase(1,0,"Eau");
                m1.changerCase(2,0,"Eau");
                Matrice m2=new Matrice(3,3);
                m2.changerCase(2,0,"Eau");
                m2.changerCase(1,1,"Eau");
                m2.changerCase(2,1,"Eau");
                m2.changerCase(0,2,"Eau");
                m2.changerCase(1,2,"Eau");
                c.setListMatrice(m1,m2);
                lc.add(c);
            } else if (i.getName().charAt(2) == 'B') {
                c=new Carte_Exploration_Unique(i, 1, "Bois");
                Matrice m1=new Matrice(2,2);
                m1.changerCase(0,0,"Bois");
                m1.changerCase(1,1,"Bois");
                Matrice m2=new Matrice(3,2);
                m2.changerCase(0,0,"Bois");
                m2.changerCase(1,0,"Bois");
                m2.changerCase(1,1,"Bois");
                m2.changerCase(2,1,"Bois");
                c.setListMatrice(m1,m2);
                lc.add(c);
            } else if (i.getName().charAt(2) == 'F') {
                c=new Carte_Exploration_Unique(i, 1, "Ferme");
                Matrice m1=new Matrice(2,1);
                m1.changerCase(0,0,"Ferme");
                m1.changerCase(1,0,"Ferme");
                Matrice m2=new Matrice(3,3);
                m2.changerCase(0,1,"Ferme");
                m2.changerCase(1,0,"Ferme");
                m2.changerCase(1,1,"Ferme");
                m2.changerCase(1,2,"Ferme");
                m2.changerCase(2,1,"Ferme");
                c.setListMatrice(m1,m2);
                lc.add(c);
            } else if (i.getName().charAt(2) == 'M') {
                c=new Carte_Exploration_Unique(i, 1, "Maison");
                Matrice m1=new Matrice(2,2);
                m1.changerCase(0,0,"Maison");
                m1.changerCase(1,0,"Maison");
                m1.changerCase(1,1,"Maison");
                Matrice m2=new Matrice(2,3);
                m2.changerCase(0,0,"Maison");
                m2.changerCase(0,1,"Maison");
                m2.changerCase(0,2,"Maison");
                m2.changerCase(1,0,"Maison");
                m2.changerCase(1,1,"Maison");
                c.setListMatrice(m1,m2);
                lc.add(c);
            }
        }
        return lc;
    }

    public ArrayList<Carte_De_Jeu> Explo_Choix(){
        String chemin="Cartes\\Exploration\\Choix";
        File p=new File(chemin);
        File[] dossier=p.listFiles();
        ArrayList<File> ls=new ArrayList<>();
        ArrayList<Carte_De_Jeu> lc=new ArrayList<>();

        assert dossier != null;
        Collections.addAll(ls, dossier);

        ls=getOnlyUse(ls);

        for(File i : ls) {

            if (i.getName().equals("CUse.jpg")) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i,0);
                c.getListChoix().add("Eau");
                c.getListChoix().add("Bois");
                c.getListChoix().add("Ferme");
                c.getListChoix().add("Maison");
                c.getListChoix().add("Monstre");
                Matrice m= new Matrice(1,1);
                m.changerCase(0,0,"Bois");
                c.setM(m);
                this.carteImpossible = c;
                lc.add(c);
            } else if (i.getName().equals("B_EUse.jpg") ) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i,2);
                c.getListChoix().add("Eau");
                c.getListChoix().add("Bois");
                Matrice m= new Matrice(3,3);
                m.changerCase(0,0,"Bois");
                m.changerCase(1,0,"Bois");
                m.changerCase(2,0,"Bois");
                m.changerCase(1,1,"Bois");
                m.changerCase(1,2,"Bois");
                c.setM(m);
                lc.add(c);
            } else if (i.getName().equals("B_FUse.jpg")) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i,2);
                c.getListChoix().add("Ferme");
                c.getListChoix().add("Bois");
                Matrice m= new Matrice(2,3);
                m.changerCase(0,0,"Bois");
                m.changerCase(0,1,"Bois");
                m.changerCase(0,2,"Bois");
                m.changerCase(1,2,"Bois");
                c.setM(m);
                lc.add(c);
            } else if (i.getName().equals("B_MUse.jpg")) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i,2);
                c.getListChoix().add("Maison");
                c.getListChoix().add("Bois");
                Matrice m= new Matrice(2,4);
                m.changerCase(0,2,"Bois");
                m.changerCase(0,3,"Bois");
                m.changerCase(1,0,"Bois");
                m.changerCase(1,1,"Bois");
                m.changerCase(1,2,"Bois");
                c.setM(m);
                lc.add(c);
            }
            else if (i.getName().equals("F_EUse.jpg")) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i, 2);
                c.getListChoix().add("Ferme");
                c.getListChoix().add("Eau");
                Matrice m= new Matrice(3,3);
                m.changerCase(0,0,"Eau");
                m.changerCase(1,0,"Eau");
                m.changerCase(2,0,"Eau");
                m.changerCase(0,1,"Eau");
                m.changerCase(0,2,"Eau");
                c.setM(m);
                lc.add(c);
            }
            else if (i.getName().equals("M_EUse.jpg")) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i, 2);
                c.getListChoix().add("Maison");
                c.getListChoix().add("Eau");
                Matrice m= new Matrice(1,4);
                m.changerCase(0,0,"Eau");
                m.changerCase(0,1,"Eau");
                m.changerCase(0,2,"Eau");
                m.changerCase(0,3,"Eau");
                c.setM(m);
                lc.add(c);
            }
            else if (i.getName().equals("M_FUse.jpg")) {
                Carte_Exploration_Choix c = new Carte_Exploration_Choix(i, 2);
                c.getListChoix().add("Maison");
                c.getListChoix().add("Ferme");
                Matrice m= new Matrice(3,2);
                m.changerCase(0,0,"Maison");
                m.changerCase(1,0,"Maison");
                m.changerCase(2,0,"Maison");
                m.changerCase(1,1,"Maison");
                c.setM(m);
                lc.add(c);
            }
        }
        return lc;
    }

    public ArrayList<Carte_Monstre> Monstre(){
        String chemin="Cartes\\Monstres";
        File p=new File(chemin);
        File[] dossier=p.listFiles();
        ArrayList<File> ls=new ArrayList<>();
        ArrayList<Carte_Monstre> lc=new ArrayList<>();

        assert dossier != null;
        Collections.addAll(ls, dossier);

        ls=getOnlyUse(ls);

        for(File i : ls) {
            if(i.getName().equals("01Use.jpg")){
                Carte_Monstre c =new Carte_Monstre(i,true,4);
                Matrice m = new Matrice(3,3);
                m.changerCase(0,0,"Monstre");
                m.changerCase(1,1,"Monstre");
                m.changerCase(2,2,"Monstre");
                c.setM(m);
                lc.add(c);
            }

            else if(i.getName().equals("02Use.jpg")){
                Carte_Monstre c =new Carte_Monstre(i,false,2);
                Matrice m = new Matrice(2,3);
                m.changerCase(0,0,"Monstre");
                m.changerCase(1,0,"Monstre");
                m.changerCase(0,2,"Monstre");
                m.changerCase(1,2,"Monstre");
                c.setM(m);
                lc.add(c);
            }
            else if(i.getName().equals("03Use.jpg")){
                Carte_Monstre c =new Carte_Monstre(i,false,3);
                Matrice m = new Matrice(3,2);
                m.changerCase(0,0,"Monstre");
                m.changerCase(1,0,"Monstre");
                m.changerCase(1,1,"Monstre");
                m.changerCase(2,0,"Monstre");
                c.setM(m);
                lc.add(c);
            }
            else if(i.getName().equals("04Use.jpg")){
                Carte_Monstre c =new Carte_Monstre(i,true,1);
                Matrice m = new Matrice(3,2);
                m.changerCase(0,0,"Monstre");
                m.changerCase(1,0,"Monstre");
                m.changerCase(2,0,"Monstre");
                m.changerCase(0,1,"Monstre");
                m.changerCase(2,1,"Monstre");
                c.setM(m);
                lc.add(c);
            }
        }
        return lc;
    }

    public ArrayList<Carte_De_Jeu> Ruine(){
        String chemin="Cartes\\Ruines";
        File p=new File(chemin);
        File[] dossier=p.listFiles();
        ArrayList<File> ls=new ArrayList<>();
        ArrayList<Carte_De_Jeu> lc=new ArrayList<>();

        assert dossier != null;
        Collections.addAll(ls, dossier);

        ls=getOnlyUse(ls);

        for(File i : ls) {
            lc.add(new Carte_Ruine(i));
        }
        return lc;
    }

    public ArrayList<Carte> Decret(){
        String chemin="Cartes\\Decret";
        File p=new File(chemin);
        File[] dossier=p.listFiles();
        ArrayList<File> ls=new ArrayList<>();
        ArrayList<Carte> lc=new ArrayList<>();

        assert dossier != null;
        Collections.addAll(ls, dossier);

        ls=getOnlyUse(ls);

        Decret c;
        for(File f : ls){
            if (f.getName().equals("26Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque espace de forêt adjacent <br>à un bord de map. </p> </HTML>");
                Matrice m1 = new Matrice(4, 4);
                m1.changerCase(0, 0, "Bois");
                m1.changerCase(0, 2, "Bois");
                m1.changerCase(0, 3, "Montagne");
                m1.changerCase(1, 0, "Bois");
                m1.changerCase(1, 1, "Bois");
                m1.changerCase(1, 2, "Bois");
                m1.changerCase(1, 3, "Montagne");
                m1.changerCase(2, 0, "Montagne");
                m1.changerCase(2, 1, "Montagne");
                m1.changerCase(2, 2, "Montagne");
                m1.changerCase(3, 2, "Montagne");
                m1.changerCase(3, 3, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("27Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque ligne et colonne avec <br>un moins un espace de forêt. <br>Un même espace de forêt peut être <br>comptabilisé dans une ligne<br> et une colonne.</p> </HTML>");
                Matrice m1 = new Matrice(3, 4);
                m1.changerCase(0, 0, "Bois");
                m1.changerCase(0, 2, "Bois");
                m1.changerCase(0, 3, "Montagne");
                m1.changerCase(1, 0, "Bois");
                m1.changerCase(1, 1, "Bois");
                m1.changerCase(1, 2, "Bois");
                m1.changerCase(1, 3, "Montagne");
                m1.changerCase(2, 0, "Montagne");
                m1.changerCase(2, 1, "Montagne");
                m1.changerCase(2, 2, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("28Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque espace de forêt <br>entouré sur ses quatres côtés <br>par un espace typé <br>ou par un bord de map.</p> </HTML>");
                Matrice m1 = new Matrice(4, 4);
                m1.changerCase(0, 0, "Bois");
                m1.changerCase(0, 2, "Bois");
                m1.changerCase(0, 3, "Montagne");
                m1.changerCase(1, 0, "Bois");
                m1.changerCase(1, 1, "Bois");
                m1.changerCase(1, 2, "Bois");
                m1.changerCase(1, 3, "Montagne");
                m1.changerCase(2, 0, "Montagne");
                m1.changerCase(2, 1, "Montagne");
                m1.changerCase(2, 2, "Montagne");
                m1.changerCase(3, 2, "Montagne");
                m1.changerCase(3, 3, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("30Use.jpg")){
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque espace d'eau <br>adjacent par au moins <br>un espace de ferme et inversement.</p> </HTML>");
                Matrice m1 = new Matrice(2, 2);
                m1.changerCase(0, 0, "Ferme");
                m1.changerCase(0, 1, "Eau");
                m1.changerCase(1, 0, "Eau");
                m1.changerCase(1, 1, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("31Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez deux points de réputation <br>pour chaque espace d'eau <br>adjacent à un espace de montagne <br>et gagnez un point de <br>réputation pour chaque espace <br>de ferme adjacent à un <br>espace de montagne.</p> </HTML>");
                Matrice m1 = new Matrice(3, 3);
                m1.changerCase(0, 0, "Ferme");
                m1.changerCase(0, 1, "Eau");
                m1.changerCase(0, 2, "Montagne");
                m1.changerCase(1, 0, "Eau");
                m1.changerCase(1, 1, "Montagne");
                m1.changerCase(1, 2, "Ferme");
                m1.changerCase(2, 0, "Eau");
                m1.changerCase(2, 1, "Ferme");
                m1.changerCase(2, 2, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("32Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque espace d'eau adjacent <br>à un espace de ruine et <br>gagner trois points de <br>réputation pour chaque espace <br>de ferme sur un espace <br>de ruine.</p> </HTML>");
                Matrice m1 = new Matrice(3, 3);
                m1.changerCase(0, 0, "Ferme");
                m1.changerCase(0, 1, "Eau");
                m1.changerCase(0, 2, "Eau");
                m1.changerCase(1, 0, "Ferme");
                m1.changerCase(1, 1, "Temple");
                m1.changerCase(1, 2, "Montagne");
                m1.changerCase(2, 0, "Montagne");
                m1.changerCase(2, 1, "Temple");
                m1.changerCase(2, 2, "Eau");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("33Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez trois points de réputation <br>pour chaque groupe d'espace <br>de ferme qui n'est <br>pas adjacent à un espace <br>d'eau ou au bord de <br>map, et de même pour un <br>espace d'eau.</p> </HTML>");
                Matrice m1 = new Matrice(5, 5);
                m1.changerCase(0, 0, "Ferme");
                m1.changerCase(0, 1, "Ferme");
                m1.changerCase(1, 2, "Eau");
                m1.changerCase(1, 3, "Ferme");
                m1.changerCase(2, 0, "Montagne");
                m1.changerCase(2, 1, "Eau");
                m1.changerCase(2, 2, "Eau");
                m1.changerCase(2, 4, "Eau");
                m1.changerCase(3, 4, "Eau");
                m1.changerCase(4, 1, "Ferme");
                m1.changerCase(4, 2, "Ferme");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("34Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez huit points de réputation <br>pour chaque groupe de six <br>espaces de maison ou plus.</p> </HTML>");
                Matrice m1 = new Matrice(3, 2);
                m1.changerCase(0, 0, "Maison");
                m1.changerCase(0, 1, "Maison");
                m1.changerCase(1, 0, "Maison");
                m1.changerCase(1, 1, "Maison");
                m1.changerCase(2, 0, "Maison");
                m1.changerCase(2, 1, "Maison");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("35Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque espace de maison <br>du plus grand groupe d'espace <br>de maison qui n'est pas <br>adjacent à un espace de montagne.</p> </HTML>");
                Matrice m1 = new Matrice(2, 4);
                m1.changerCase(0, 0, "Maison");
                m1.changerCase(0, 1, "Maison");
                m1.changerCase(1, 1, "Maison");
                m1.changerCase(1, 2, "Maison");
                m1.changerCase(1, 3, "Maison");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("36Use.jpg")) {
                Matrice m1 = new Matrice(3, 3);
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez trois points de réputation <br>pour chaque groupe d'espace <br>de maison qui est adjacent <br>à trois espaces typés <br>différent ou plus.</p> </HTML>");
                m1.changerCase(0, 2, "Montagne");
                m1.changerCase(1, 0, "Maison");
                m1.changerCase(1, 1, "Maison");
                m1.changerCase(1, 2, "Maison");
                m1.changerCase(2, 0, "Eau");
                m1.changerCase(2, 1, "Maison");
                m1.changerCase(2, 2, "Bois");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("37Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez deux points de réputation <br>pour chaque espace de maison <br>du deuxième groupe d'espace <br>de maison le plus grand.</p> </HTML>");
                Matrice m1 = new Matrice(2, 4);
                m1.changerCase(0, 0, "Maison");
                m1.changerCase(0, 1, "Maison");
                m1.changerCase(1, 1, "Maison");
                m1.changerCase(1, 2, "Maison");
                m1.changerCase(1, 3, "Maison");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("38Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez six points de réputation <br>pour chaque ligne ou <br>colonne complètement remplie <br>d'espaces typés.</p> </HTML>");
                Matrice m1 = new Matrice(3, 3);
                m1.changerCase(0, 0, "Montagne");
                m1.changerCase(0, 1, "Montagne");
                m1.changerCase(0, 2, "Montagne");
                m1.changerCase(1, 0, "Montagne");
                m1.changerCase(2, 0, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("39Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez trois points de réputation <br>pour chaque espace le long <br>du côté du plus grand <br>carré d'espaces typés.</p> </HTML>");
                Matrice m1 = new Matrice(3, 3);
                m1.changerCase(0, 0, "Montagne");
                m1.changerCase(0, 1, "Montagne");
                m1.changerCase(1, 0, "Montagne");
                m1.changerCase(1, 1, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("40Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez trois points de réputation <br>pour chaque diagonale complétement <br>remplies d'espaces typés touchant <br>le bord gauche et le <br>bas de la map.</p> </HTML>");
                Matrice m1 = new Matrice(3, 3);
                m1.changerCase(0, 0, "Montagne");
                m1.changerCase(0, 2, "Montagne");
                m1.changerCase(1, 1, "Montagne");
                m1.changerCase(2, 0, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }
            if (f.getName().equals("41Use.jpg")) {
                c = new Decret(f,"<HTML> <p style='text-align:center;'> Gagnez un point de réputation <br>pour chaque espace vide entouré <br>d'espaces typés ou du bord <br>de la map sur chacun <br>de ses quatres côtés.</p> </HTML>");
                Matrice m1 = new Matrice(3, 3);
                m1.changerCase(0, 1, "Montagne");
                m1.changerCase(1, 0, "Montagne");
                m1.changerCase(1, 2, "Montagne");
                m1.changerCase(2, 1, "Montagne");
                c.setMatrice(m1);
                lc.add(c);
            }

        }

        Collections.shuffle(lc);
        return lc;
    }

    public ArrayList<Saison> Saisons(){
        String chemin="Cartes\\Saison";  //Chemin du dossier des cartes
        File p=new File(chemin);
        File[] dossier=p.listFiles(); //Liste contenant tous les dossiers du fichier dans le chemin de la variable chemin
        ArrayList<File> ls=new ArrayList<>();
        ArrayList<Saison> lc=new ArrayList<>();

        assert dossier != null;
        Collections.addAll(ls, dossier);

        ls=getOnlyUse(ls);

        for (File i :ls){
            if (i.getName().equals("0SpringUse.png")){
                Saison s = new Saison(i,8,0);
                lc.add(s);
            }
            if (i.getName().equals("1SummerUse.png")){
                Saison s = new Saison(i,8,1);
                lc.add(s);
            }
            if (i.getName().equals("2FallUse.png")){
                Saison s = new Saison(i,7,2);
                lc.add(s);
            }
            if (i.getName().equals("3WinterUse.png")){
                Saison s = new Saison(i,6,3);
                lc.add(s);
            }
        }

        return lc;
    }


    /*
    Ajouter une carte monstre
    et remélanger après les cartes
     */
    public void ajouterMonstre(){
        Collections.shuffle(this.listMonstre);
        this.cartesdeJeu.add(this.listMonstre.get(0));
        this.listMonstre.remove(0);
        melanger();
    }

    public void melanger(){
        Collections.shuffle(cartesdeJeu);
        Collections.shuffle(cartesdeJeu);
        Collections.shuffle(cartesdeJeu);
    }

    public void Impossible(){ current_carte = carteImpossible;}


    //Getteurs
    public Saison getCurrent_saison() {return this.current_saison;}

    public Joueur getCurrent_joueur(){ return this.current_joueur; }

    public Carte_De_Jeu getCurrent_carte(){return this.current_carte;}

    public Carte_De_Jeu getExCarte(){ return this.ex_carte; }

    public Carte_De_Jeu getCarteImpossible(){ return this.carteImpossible;}

    public boolean getFin(){
        return fin;
    }

    public ArrayList<Joueur> getJoueurs(){
        return this.joueur;
    }

    public int getNbTour(){
        return nbTour;
    }

    public Jeu getJeu(){ return jeu; }

    /*
    Méthode permettant de supprimer de la liste les images qui n'ont pas 'Use' dans leur nom
    càd -> les images de taille redéfinie et qui sont à utiliser
     */
    public ArrayList<File> getOnlyUse(ArrayList<File> ls){

        ArrayList<Integer> index = new ArrayList<>();
        int fichierNum = 0;

        for(File fichier : ls){

            boolean alors = false;

            for(int i=0 ; i<fichier.getName().length() ; i++){

                if(fichier.getName().charAt(i)=='U' && fichier.getName().charAt(i+1)=='s' && fichier.getName().charAt(i+2)=='e'){
                    alors = true;
                    break;
                }
                if(fichier.getName().charAt(i)=='.'){
                    break;
                }

            }
            if(!alors) index.add(fichierNum);
            fichierNum++;

        }

        Collections.reverse(index);

        for( int i : index){
            ls.remove(i);
        }

        return ls;

    }
}