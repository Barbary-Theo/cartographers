package Model.maps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Matrice implements Serializable {
    private int ligne;
    private int colonne;
    private Case[][] matrice;

    /*
     * Création de la matrice si ce n'est pas par recopie
     * création de nouvelle Model.maps.Case par défaut (vide) en fonction du nombre de ligne et de colonne
     */
    public Matrice(int li, int co ){
        this.ligne = li;
        this.colonne = co;

        this.matrice = new Case[li][co];
        for(int i=0 ; i<ligne ; i++) {
            for (int j=0 ; j<colonne ; j++) {
                matrice[i][j] = new Case(i,j,"",0);
            }
        }
    }

    public static Matrice initMap(){

        Matrice map = new Matrice(11,11);

        map.changerCase(1,3,"Montagne");
        map.changerCase(3,8,"Montagne");
        map.changerCase(8,2,"Montagne");
        map.changerCase(5,5,"Montagne");
        map.changerCase(9,7,"Montagne");

        map.changerCase(1,5,"Temple");
        map.changerCase(2,1,"Temple");
        map.changerCase(2,9,"Temple");
        map.changerCase(8,9,"Temple");
        map.changerCase(8,1,"Temple");
        map.changerCase(9,5,"Temple");

        return map;

    }

    /*
     * Constructeur par recopie
     */
    public Matrice(int li, int co, Case[][] recopie){
        this.colonne = co;
        this.ligne = li;
        this.matrice = recopie;
    }

    public Matrice(Matrice m){
        var ex = new Matrice(m.getLigne(),m.getColonne(),m.getMatrice());
        Case[][] mat = new Case[m.getLigne()][m.getColonne()];
        for(int i=0 ; i< m.getLigne() ; i++){
            for(int j=0 ; j< m.getColonne() ; j++){

                mat[i][j] = new Case(m.getMatrice()[i][j].getX(),m.getMatrice()[i][j].getY(),m.getMatrice()[i][j].getType(),m.getMatrice()[i][j].getNbPoint(),m.getMatrice()[i][j].isCheck());

            }
        }
        ex.setMatrice(mat);
        this.ligne = ex.getLigne();
        this.colonne = ex.getColonne();
        this.matrice = ex.getMatrice();
    }

    /*
     * Getters et setters
     */
    public int getColonne() {
        return colonne;
    }

    public int getLigne() {
        return ligne;
    }

    public Case[][] getMatrice() {
        return matrice;
    }

    public void setMatrice(Case[][] matrice) {
        this.matrice = matrice;
    }

    /*
     * Affichage de la matrice pour les phases de tests et les vérifications
     * Compteur pour savoir nous sommes à quel indice dans l'attribut matrice de la classe
     * Permettant ainsi de récupérer les infos des Cases dans ce cas les coordonnées

    public void affichageMatrice() {
        for(int i=0 ; i<ligne ; i++) {
            for (int j=0 ; j<colonne ; j++) {
                System.out.print("["+matrice[i][j].getX()+";"+matrice[i][j].getY()+"] ");
            }
            System.out.println("\n");
        }

    }
*/
    /*
     * Affichage de la matrice pour les phases de tests et les vérifications
     * Compteur pour savoir nous sommes à quel indice dans l'attribut matrice de la classe
     * Permettant ainsi de récupérer les infos des Cases dans ce cas le type
     */
    public void affichageMatriceType() {

        for(int i=0 ; i<ligne ; i++) {
            for (int j=0 ; j<colonne ; j++) {
                System.out.print("["+matrice[i][j].getType()+"] ");
            }
            System.out.println("\n");
        }

    }

    /*
     * Méthode pour changer le type à la case aux coordonnées données
     */
    public void changerCase(int xx, int yy, String newType) {
        matrice[xx][yy].setType(newType);
    }

    public void rotateCarte() {

        Case[][] matt = new Case[this.getColonne()][this.getLigne()];
        Case[] ligne;

        var lignee = new ArrayList<Case>();
        var colonne = new ArrayList<ArrayList<Case>>();

        for (int k=0 ; k< this.getColonne() ;k++){
            lignee = new ArrayList<>();
            for (int i=0 ; i< this.getLigne() ; i++){
                lignee.add(this.getMatrice()[i][k]);
            }
            colonne.add(lignee);
        }

        for(ArrayList<Case> ligneee : colonne){
            Collections.reverse(ligneee);
        }

        for(int i=0 ; i<colonne.size() ; i++){
            ligne = new Case[this.getLigne()];
            for(int j=0 ; j<colonne.get(i).size() ; j++){
                ligne[j] = colonne.get(i).get(j);
            }
            matt[i] = ligne;

        }

        this.ligne = matt.length;
        this.colonne = matt[0].length ;
        matrice = matt;

    }

    public void reverseCarte(){
        var M = this;

        Matrice nouveau = new Matrice(M.getLigne(),M.getColonne());

        for(int i=0 ; i<M.getLigne() ; i++){
            for(int j=0 ; j<M.getColonne() ; j++){

                nouveau.changerCase(i, M.getColonne()-1-j,M.getMatrice()[i][j].getType());

            }
        }

        this.matrice = nouveau.getMatrice();
    }


}
