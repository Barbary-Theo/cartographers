package Model.maps;

import java.io.Serializable;

public class Case implements Serializable {
    private final int x;
    private final int y;
    private String type;
    private final int nbPoint;
    private boolean check;

    public Case(int xx, int yy, String typ, int nb){

        this.x = xx;
        this.y = yy;

        if(typ.equals("Monstre") || typ.equals("Maison") || typ.equals("Eau") || typ.equals("Ferme") || typ.equals("Bois") || typ.equals("Temple") || typ.equals("Montagne")) {
            this.type = typ;
        }
        else this.type = "Vide";

        this.nbPoint = nb;
        check = false;
    }

    public Case(int xx, int yy, String typ, int nb, boolean check){

        this.x = xx;
        this.y = yy;

        if(typ.equals("Monstre") || typ.equals("Maison") || typ.equals("Eau") || typ.equals("Ferme") || typ.equals("Bois") || typ.equals("Temple") || typ.equals("Montagne")) {
            this.type = typ;
        }
        else this.type = "Vide";

        this.nbPoint = nb;
        this.check = check;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getType() {
        return type;
    }

    public void setType(String typ) {
        if(typ.equals("Monstre") || typ.equals("Maison") || typ.equals("Eau") || typ.equals("Ferme") || typ.equals("Bois") || typ.equals("Temple") || typ.equals("Montagne")) {
            this.type = typ;
        }
        else this.type = "Vide";
    }

    public int getNbPoint() {
        return nbPoint;
    }

    public void setCheck(boolean che){
        this.check = che;
    }

    public boolean isCheck(){
        return this.check;
    }

}
