package Controller.events;
import Model.Music.PoserMusic;
import Model.cartes.Carte_De_Jeu;
import Model.cartes.Carte_Exploration_Choix;
import Model.cartes.Carte_Exploration_Unique;
import View.*;

import Controller.*;
import View.Panels.JPanelTransparent;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;


import javax.swing.*;

import static java.awt.Component.CENTER_ALIGNMENT;

public class MonMouseListener implements MouseListener{
    public Carte_De_Jeu carte;//récupération de la carte du tour
    public int i;// l'indice de la ligne sur laquelle on a cliqué (map du joueur)
    public int j;// l'indice de la colonne sur laquelle on a cliqué (map du joueur)
    public int num;// numéro de la matrice cliquée
    public MyMouseListener even;// numéro de la matrice cliquée

    public MonMouseListener(MyMouseListener even, int i, int j) {

        if(even!=null){
            this.i=i;
            this.j=j;
            this.carte = even.carte;
            this.num = even.num;
            this.even = even;
        }

    }

    public void mousePressed(MouseEvent evt) {

    }
    public void mouseReleased(MouseEvent evt) {

    }
    public void mouseEntered(MouseEvent evt) {

    }

    public void mouseExited(MouseEvent evt) {

    }

    //évènement quand on clique sur la case
    public void mouseClicked(MouseEvent evt) {
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        if(carte!=null) {//Si la carte n'est pas null :: plus concrétement si on a bien cliqué sur la matrice de la carte avec la matrice du joueur

            Vue.map.removeAll();//suppression de la map du joueur

            //Initialisation de toutes les valeurs et de la forme
            boolean toDo;
            JButton valider = new JButton("Valider");//Valider et annuler le placement d'une zone
            JButton annuler = new JButton("Annuler");
            JPanel toAdd = new JPanel();

            ((JPanel) ((JPanel) Vue.right.getComponent(0)).getComponent(2)).removeAll();
            JPanel ConvenirAndSpace = new JPanel(); ConvenirAndSpace.setLayout(new BoxLayout(ConvenirAndSpace,BoxLayout.Y_AXIS));
            JLabel convenir = new JLabel("Cette position vous convient t-elle ?");convenir.setAlignmentX(CENTER_ALIGNMENT);
            ConvenirAndSpace.add(convenir); ConvenirAndSpace.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));
            ((JPanel) ((JPanel) Vue.right.getComponent(0)).getComponent(2)).add(ConvenirAndSpace,BorderLayout.NORTH);
            toAdd.add(valider);
            toAdd.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
            toAdd.add(annuler);
            ((JPanel) ((JPanel) Vue.right.getComponent(0)).getComponent(2)).add(toAdd);

            toAdd.setBackground(new Color(207,193,175));
            ConvenirAndSpace.setBackground(new Color(207,193,175));

            valider.addActionListener(e -> {

                if(carte instanceof Carte_Exploration_Unique && num==0){
                    Controller.addPiece();
                }

                Vue.map.removeAll();//Suprimer tous les éléments
                Vue.gc.gridx = 2;
                Vue.gc.gridy = 3;
                var m = Vue.getMap();
                Vue.jf.add(m, Vue.gc);//Ajouter la nouvelle matrice du joueur au Frame
                Vue.map = m;

                ((JPanel) ((JPanel) Vue.right.getComponent(0)).getComponent(2)).removeAll();

                JPanel SpaceAndTermine = new JPanel();SpaceAndTermine.setLayout(new BoxLayout(SpaceAndTermine,BoxLayout.X_AXIS));
                SpaceAndTermine.setBackground(new Color(207,193,175));
                SpaceAndTermine.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                JButton termine = new JButton("Terminer le tour");
                SpaceAndTermine.add(termine);
                ((JPanel) ((JPanel) Vue.right.getComponent(0)).getComponent(2)).add(SpaceAndTermine);
                Vue.jf.repaint();//refresh la page
                Vue.jf.setVisible(true);

                termine.addMouseListener( new EventEndTurn());


            });


            annuler.addActionListener(e -> {
                Vue.map.removeAll();//Supprimer tous les éléments
                Vue.gc.gridx = 2;
                Vue.gc.gridy = 3;

                var n = Controller.getCurrent_Joueur().getEx_Matrice_Joueur();
                Controller.getCurrent_Joueur().setMatrice_Joueur(n);
                var p = Vue.getMap();
                Vue.jf.add(p, Vue.gc);//Dire que la matrice du joueur est celle d'avant, car l'action a été annulée
                Vue.map = p;

                Vue.right.removeAll();//supprimer la carte + les matrices
                JPanel right = null;//Récupération du panel de la carte + des matrices + plus boutton rotate
                try {
                    right = Vue.getCarteTour();
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                JPanel finale = new JPanel(); finale.add(right);
                finale.setBackground(new Color(207,193,175));
                Vue.gc.gridx = 5;
                Vue.gc.gridy = 3;
                Vue.jf.add(finale, Vue.gc);//Ajout des éléments dans la vue
                Vue.right = finale;

                Vue.jf.repaint();//refresh la page
                Vue.jf.setVisible(true);


            });

            if (carte instanceof Carte_Exploration_Choix) {//Si c'est une CarteExplorationChoix

                for (int a = 0; a < carte.getM().getLigne(); a++) {
                    for (int b = 0; b < carte.getM().getColonne(); b++) {
                        if (!carte.getM().getMatrice()[a][b].getType().equals("Vide")) {
                            carte.getM().getMatrice()[a][b].setType(((Carte_Exploration_Choix) carte).getListChoix().get(num));//Création d'une matrice avec le bon type à placer
                        }
                    }
                }
                toDo = Controller.getCurrent_Joueur().poserZoneUnique(carte.getM(),"Choix", i, j);//Poser la matrice sur la map du joueur

            } else if (carte instanceof Carte_Exploration_Unique) {//Si c'est une CarteExplorationUnique
                toDo = Controller.getCurrent_Joueur().poserZoneUnique(((Carte_Exploration_Unique) carte).getListMatrice().get(num),"Unique", i, j);//Poser la matrice sur la map du joueur

            } else {//Si c'est Monstre
                toDo = Controller.getCurrent_Joueur().poserZoneUnique(carte.getM(),"Monstre", i, j);//Poser la matrice sur la map du joueur

            }

            //Si le placement de la zone est correcte
            Vue.gc.gridx = 2;
            Vue.gc.gridy = 3;
            JPanel add;//refresh la page
            if (toDo) {
                add = getMap2(i, j);
                Vue.jf.add(add, Vue.gc);//Placer la nouvelle map
                Vue.map = add;

                Thread music = new PoserMusic();
                music.start();
            }
            else {

                add = Vue.getMap();
                Vue.jf.add(add, Vue.gc);//Placer la nouvelle map
                Vue.map = add;

                Vue.right.removeAll();//Supprimer tous les éléments de droites (carte + matrices + boutton)

                JPanel right = null;//Récupération du panel de la carte, des matrices et du boutons
                try {
                    right = Vue.getCarteTour();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                JPanel ImpossibleAndSpace = new JPanel();ImpossibleAndSpace.setLayout(new BoxLayout(ImpossibleAndSpace,BoxLayout.Y_AXIS));//panel label erreur + un espace
                JLabel impossible = new JLabel("Impossible de placer à cette possition");impossible.setAlignmentX(CENTER_ALIGNMENT);//Création du lavel (indication au centre)
                ImpossibleAndSpace.add(impossible);ImpossibleAndSpace.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));//Ajout des éléments dans le panel
                ((JPanel) right.getComponent(2)).add(ImpossibleAndSpace,BorderLayout.NORTH);//Ajout au panel droit de la vue (partie Nord)
                JPanel finale = new JPanel(); finale.add(right);

                add.setBackground(new Color(207,193,175));
                ImpossibleAndSpace.setBackground(new Color(207,193,175));
                finale.setBackground(new Color(207,193,175));

                Vue.gc.gridx = 5;
                Vue.gc.gridy = 3;
                Vue.jf.add(finale, Vue.gc);//Ajout à la vue droit
                Vue.right = finale;

            }
            Vue.jf.repaint();//refresh la page
            Vue.jf.setVisible(true);


        }


    }

    //Méthode pour récupérer le panel de la matrice du joueur de tel sort que la zone venant de placer ne sooit pas transparant
    public JPanel getMap2( int y, int x){
        //initialisation
        JPanel global = new JPanel();global.setLayout(new BoxLayout(global,BoxLayout.Y_AXIS));
        JPanel conteneur;
        JLabel image;
        File fichier;
        JPanel f;

        //Affichage de la matrice de la map du joueur passé en en paramètre
        var matrice=Controller.getCurrent_Joueur().getMatrice_Joueur();
        var liste=matrice.getMatrice();
        for(int i=0 ; i<matrice.getLigne() ; i++){
            conteneur = new JPanel();//panel ligne
            for(int j=0 ; j<matrice.getColonne() ; j++){
                if(carte instanceof Carte_Exploration_Unique){//Si c'est une carteExplorationUnique
                    if((x <= j && j <= x + ((Carte_Exploration_Unique) carte).getListMatrice().get(num).getColonne() - 1)//Vérification que la case actuelle est une case de la zone qui vient d'être placer
                            && (y <= i && i <= y + ((Carte_Exploration_Unique) carte).getListMatrice().get(num).getLigne() - 1)
                    && Controller.getCurrent_Joueur().getMatrice_Joueur().getMatrice()
                            [i][j].getType().equals(((Carte_Exploration_Unique) carte).getType())){
                        f = new JPanel();//panel normale
                    } else {
                        f = new JPanelTransparent();//Si c'est pas une case comme dit précédemment -> panel transparent
                    }
                }
                else if(carte instanceof Carte_Exploration_Choix){//Si c'est une carteExplorationChoix
                    if ((x <= j && j <= x + carte.getM().getColonne() - 1) && (y <= i && i <= y + carte.getM().getLigne() - 1) &&//Vérification que la case actuelle est une case de la zone qui vient d'être placer
                    Controller.getCurrent_Joueur().getMatrice_Joueur().getMatrice()[i][j].getType().equals(((Carte_Exploration_Choix) carte).getListChoix().get(num))) {
                        f = new JPanel();//panel normale
                    } else {
                        f = new JPanelTransparent();//Si c'est pas une case comme dit précédemment -> panel transparent
                    }
                }
                else {//Si c'est une carte Monstre
                    if ((x <= j && j <= x + carte.getM().getColonne() - 1) && (y <= i && i <= y + carte.getM().getLigne() - 1)//Vérification que la case actuelle est une case de la zone qui vient d'être placer
                            && Controller.getCurrent_Joueur().getMatrice_Joueur().getMatrice()[i][j].getType().equals("Monstre")) {
                        f = new JPanel();//panel normale
                    } else {
                        f = new JPanelTransparent();//Si c'est pas une case comme dit précédemment -> panel transparent
                    }
                }


                fichier=Controller.icone(liste[i][j].getType());//Récupération des images
                assert fichier != null;
                image =new JLabel(new ImageIcon(fichier.getPath()));
                f.add(image);

                conteneur.add(f);f.setBackground(new Color(207,193,175));
                f.setLayout(new BoxLayout(f,BoxLayout.X_AXIS));
                conteneur.setLayout(new BoxLayout(conteneur,BoxLayout.X_AXIS));
            }
            global.add(conteneur);//ajouter le panel ligne au tout
            conteneur.setBackground(new Color(207,193,175));
        }

        return global;
    }



}