package Controller.events;

import Controller.Controller;
import Model.cartes.Decret;
import Model.cartes.Saison;
import View.Vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class DecretEvents implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        ((JPanel) ((JPanel) Vue.saisonDecret.getComponent(0)).getComponent(1)).removeAll();

        JLabel text;
        Saison s = Controller.getCurrent_saison();
        for(Decret d : s.getDecrets()){// Pour tout les décrets
            JPanel SpaceAndDescirption = new JPanel(); SpaceAndDescirption.setLayout(new BoxLayout(SpaceAndDescirption,BoxLayout.Y_AXIS));
            text=new JLabel(d.getDescription());//Récupération de l'image des cartes
            SpaceAndDescirption.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));SpaceAndDescirption.add(Box.createRigidArea(new Dimension(0, (height*50)/1080)));SpaceAndDescirption.add(text);
            ((JPanel) ((JPanel) Vue.saisonDecret.getComponent(0)).getComponent(1)).add(SpaceAndDescirption);
            SpaceAndDescirption.setBackground(new Color(207,193,175));
        }

        Vue.jf.repaint();
        Vue.jf.setVisible(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        ((JPanel) Vue.saisonDecret.getComponent(0)).remove(1);

        JPanel oui = Vue.getDecrets();
        oui.setBackground(new Color(207,193,175));
        ((JPanel) Vue.saisonDecret.getComponent(0)).add(oui);

        Vue.jf.repaint();
        Vue.jf.setVisible(true);
    }



}
