package Controller.events;
import Model.cartes.Carte_De_Jeu;
import Model.cartes.Carte_Exploration_Choix;
import Model.cartes.Carte_Exploration_Unique;
import Model.maps.Matrice;
import View.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

//Evènement pour rotate les matrices
public class Reverse implements MouseListener {
    public JPanel global;//panel des matrices
    public Carte_De_Jeu carte;//la carte du tour

    public Reverse( JPanel pounn, Carte_De_Jeu car){
        this.global = pounn;
        this.carte = car;
    }

    //Quand on clique sur le boutton rotate
    @Override
    public void mouseClicked(MouseEvent e) {
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        int i;

        global.removeAll();//Supprimer les matrices
        global.add(Box.createRigidArea(new Dimension((width*50)/1920,0)));//espace
        if(carte instanceof Carte_Exploration_Choix){//si c'est une carte choix
            if(carte.getM()!=null) {
                var m = carte.getM();
                m.reverseCarte();//Rotate la matrice
                i=0;
                for (String type : ((Carte_Exploration_Choix) carte).getListChoix()) {
                    var oui = Vue.getMatriceTour(carte.getM(), type);
                    oui.addMouseListener(new MyMouseListener(carte, i));
                    global.add(oui);//ajout de la matrice
                    oui.setBackground(new Color(207,193,175));
                    global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                    i++;
                }
            }
        }//Si c'est une carte Explo unique deux matrices du même type
        else if(carte instanceof Carte_Exploration_Unique) {
            i=0;
            for(Matrice m : ((Carte_Exploration_Unique) carte).getListMatrice()){
                m.reverseCarte();//Rotate la matrice
                var oui = Vue.getMatriceUnique(m,((Carte_Exploration_Unique) carte).getType());
                oui.addMouseListener(new MyMouseListener(carte, i));
                global.add(oui);//ajout de la matrice
                oui.setBackground(new Color(207,193,175));
                global.add(Box.createRigidArea(new Dimension((width*50)/1920,0)));
                i++;
            }
        }//Si monstre ou ruine
        else {
            if(carte.getM()!=null) {
                var m = carte.getM();
                m.reverseCarte();//Rotate la matrice
                var oui = Vue.getMatriceTour(carte.getM(), "Monstre");
                oui.addMouseListener(new MyMouseListener(carte,0));
                global.add(oui);
                oui.setBackground(new Color(207,193,175));
            }
        }

        global.setBackground(new Color(207,193,175));

        Vue.jf.repaint();//refresh
        Vue.jf.setVisible(true);

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
