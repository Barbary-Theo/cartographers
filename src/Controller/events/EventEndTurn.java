package Controller.events;

import Controller.*;
import Model.Jeu.JeuMulti;
import Model.Jeu.JeuSolo;
import Model.Music.MoneyMusic;
import View.Vue;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

public class EventEndTurn implements MouseListener {
    @Override
    public void mouseClicked(MouseEvent e) {
        Thread music = new MoneyMusic();
        music.start();

        if(Controller.getJeu() instanceof JeuSolo){
            Controller.EndTurnSolo();
        }
        else if(Controller.getJeu() instanceof JeuMulti){
            Controller.EndTurnMulti();//Changer le joueur actuel
        }
        else {
            try {
                Controller.EndTurnIA();//Changer le joueur actuel
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }

        if(!Controller.getFin()){

            try {
                Vue.repaintTour();//refresh les éléments
            } catch (IOException | InterruptedException ioException) {
                ioException.printStackTrace();
            }

            Vue.jf.repaint();//refresh la page
            Vue.jf.setVisible(true);

        }
    }

    @Override
    public void mousePressed(MouseEvent e) {


    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
