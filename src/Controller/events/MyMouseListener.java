package Controller.events;
import Controller.Controller;
import Model.cartes.Carte_De_Jeu;
import Model.cartes.Carte_Exploration_Choix;
import Model.cartes.Carte_Exploration_Unique;
import Model.maps.Matrice;
import View.*;
import View.Panels.JPanelTransparent;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.*;

public class MyMouseListener implements MouseListener{
    public Carte_De_Jeu carte;//La carte du tour
    public int num;//Le numéro de la matrice cliquée (Si deux types ou deux matrices, cf les différentes cartes)


    public MyMouseListener( Carte_De_Jeu carte, int numm) {
         this.carte = carte;
         num = numm;
    }

    public void mousePressed(MouseEvent evt) {

    }
    public void mouseReleased(MouseEvent evt) {

    }
    public void mouseEntered(MouseEvent evt) {

    }
    public void mouseExited(MouseEvent evt) {

    }
    //Quand on a clické
    public void mouseClicked(MouseEvent evt) {


        var finale = convertToJP();//Méthode pour récupérer le Panel non Transparant de la matrice sur laquelle l'utilisateur a cliqué + la matrice Transparante de la non-cliquée

        ((JPanel)((JPanel)((JPanel) Vue.right.getComponent(0)).getComponent(2)).getComponent(1)).removeAll();//supression des deux (ou de la) matrices de la carte

        ((JPanel)((JPanel)((JPanel) Vue.right.getComponent(0)).getComponent(2)).getComponent(1)).add(finale);//Ajout au même endroit des deux matrices récupérer 4 lignes au dessus


        Vue.jf.repaint();//refresh de la page
        Vue.jf.setVisible(true);


        Controller.event = this;//Indique que le dernière évnènement est celui là, on pourra alors récupérer la matrice sur laquel on a cliquée
        Vue.map.removeAll();//Suprimer tous les éléments
        Vue.gc.gridx = 2;
        Vue.gc.gridy = 3;
        var m = Vue.getMap();
        Vue.jf.add(m,Vue.gc);//Ajouter la nouvelle matrice du joueur au Frame
        Vue.map = m;

        Vue.jf.repaint();//refresh la page
        Vue.jf.setVisible(true);

    }

    /*
    Les méthodes suivantes permettront, comme dans la classe vue,
    de récupérer les matrices mais cette fois en JPanel, transparent si pas cliqué ou normal si cliqué
    (Les mêmes que getCarteTour, getMatriceTour, getMatriceUnique)
     */
    public JPanel convertToJP(){
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();

        JPanel global = new JPanel();

        int i;

        //Si c'est une carte Explo choix, alors il y a la même matrice de différents types
        global.add(Box.createRigidArea(new Dimension((width*50)/1920,0)));
        if(carte instanceof Carte_Exploration_Choix){
            i = 0;
            if(carte.getM()!=null) {
                for (String type : ((Carte_Exploration_Choix) carte).getListChoix()) {
                    var oui = getMatriceTour(carte.getM(), type);
                    oui.addMouseListener(new MyMouseListener(carte, i));
                    oui.setBackground(new Color(207,193,175));
                    global.add(oui);
                    global.add(Box.createRigidArea(new Dimension((width*50)/1920, 0)));
                    i++;
                }
            }
        }//Si c'est une carte Explo unique deux matrices du même type
        else if(carte instanceof Carte_Exploration_Unique) {
            i = 0;
            for(Matrice m : ((Carte_Exploration_Unique) carte).getListMatrice()){
                var oui = getMatriceUnique(m,((Carte_Exploration_Unique) carte).getType());
                oui.addMouseListener(new MyMouseListener(carte, i));
                oui.setBackground(new Color(207,193,175));
                global.add(oui);
                global.add(Box.createRigidArea(new Dimension((width*50)/1920,0)));
                i++;
            }
        }//Si monstre ou ruine
        else {
            if(carte.getM()!=null) {
                var oui = getMatriceTour(carte.getM(), "Monstre");
                oui.addMouseListener(new MyMouseListener(carte,0));
                oui.setBackground(new Color(207,193,175));
                global.add(oui);
            }
        }
        global.setLayout(new BoxLayout(global,BoxLayout.X_AXIS));

        Icon icon = new ImageIcon("Icone\\rotate.PNG");
        JButton button = new JButton(icon);
        button.addMouseListener(new Rotate(global, carte));

        global.setBackground(new Color(207,193,175));

        return global;
    }

    public JPanel getMatriceTour(Matrice matricee, String type){
        //Initialisation
        JPanel conteneur;
        JLabel image;
        File fichier;
        JPanel leftIn = new JPanel();

        //En gros si c'est pas une carte ruine on peut faire la matrice

        if(carte instanceof Carte_Exploration_Choix){
            if(((Carte_Exploration_Choix) carte).getListChoix().size()==5){
                if((type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(0)) && num==0) || (type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(1)) && num==1) ||
                (type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(2)) && num==2) ||(type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(3)) && num==3) ||
                        (type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(4)) && num==4)){
                    leftIn = new JPanel();
                }
                else {
                    leftIn = new JPanelTransparent();
                }
            }
            else {
                if ((type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(0)) && num == 0) || (type.equals(((Carte_Exploration_Choix) carte).getListChoix().get(1)) && num == 1)) {
                    leftIn = new JPanel();
                } else {
                    leftIn = new JPanelTransparent();
                }
            }
        }
        leftIn.setLayout(new BoxLayout(leftIn,BoxLayout.Y_AXIS));
        for(int i=0 ; i<matricee.getLigne() ; i++){
            conteneur = new JPanel();//panel ligne
            for(int j=0 ; j<matricee.getColonne() ; j++){
                if(matricee.getMatrice()[i][j].getType().equals("Vide")){
                    fichier = new File("Icone\\TransparantUse.PNG");
                }
                else {
                    fichier = new File("Icone\\"+type+"Use.PNG");
                }
                image =new JLabel(new ImageIcon(fichier.getPath()));
                conteneur.add(image,BorderLayout.CENTER);
                conteneur.setLayout(new BoxLayout(conteneur,BoxLayout.X_AXIS));
            }
            leftIn.add(conteneur);//ajouter le panel ligne au tout
            conteneur.setBackground(new Color(207,193,175));
        }

        leftIn.setBackground(new Color(207,193,175));

        return leftIn;
    }

    public JPanel getMatriceUnique(Matrice matricee, String type){
        JPanel conteneur;
        JLabel image;
        File fichier;
        JPanel leftIn = null;

        if(carte instanceof Carte_Exploration_Unique){
            if((matricee.equals(((Carte_Exploration_Unique) carte).getListMatrice().get(0)) && num==0) || (matricee.equals(((Carte_Exploration_Unique) carte).getListMatrice().get(1)) && num==1)){
                leftIn = new JPanel();
            }
            else {
                leftIn = new JPanelTransparent();
            }
        }

        assert leftIn != null;
        leftIn.setLayout(new BoxLayout(leftIn,BoxLayout.Y_AXIS));

        for(int i=0 ; i<matricee.getLigne() ; i++){
            conteneur = new JPanel();//panel ligne
            for(int j=0 ; j<matricee.getColonne() ; j++){
                if(matricee.getMatrice()[i][j].getType().equals("Vide")){
                    fichier = new File("Icone\\TransparantUse.PNG");
                }
                else {
                    fichier = new File("Icone\\"+type+"Use.PNG");
                }
                image =new JLabel(new ImageIcon(fichier.getPath()));
                conteneur.add(image,BorderLayout.CENTER);
                conteneur.setLayout(new BoxLayout(conteneur,BoxLayout.X_AXIS));
            }
            leftIn.add(conteneur);//ajouter le panel ligne au tout
            conteneur.setBackground(new Color(207,193,175));
        }

        leftIn.setBackground(new Color(207,193,175));
        
        return leftIn;
    }


}

