package Controller;
import Controller.events.MyMouseListener;
import Model.Jeu.Jeu;
import Model.Jeu.JeuIA;
import Model.Jeu.JeuMulti;
import Model.Jeu.JeuSolo;
import Model.cartes.Carte_De_Jeu;
import Model.cartes.Carte_Monstre;
import Model.cartes.Saison;
import Model.maps.Matrice;
import Model.players.Joueur;
import View.Vue;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;


public class Controller {

    private static Jeu jeu;//La partie en cours
    public static MyMouseListener event;//Le dernier événement (pour avoir la matrice cliquée)


    //tirer une carte au hasard
    public static Carte_De_Jeu getCarte(){
        return jeu.getCurrent_carte();
    }

    //Chemin du type passé en paramètre
    public static File icone(String s){
        return new File("Icone\\" + s + "Use.PNG");
    }

    //la saison actuelle
    public static Saison getCurrent_saison(){return jeu.getCurrent_saison();}

    public static Joueur getCurrent_Joueur(){
        return jeu.getCurrent_joueur();
    }

    public static ArrayList<Joueur> getJoueurs(){  return jeu.getJoueurs();  }

    public static Carte_De_Jeu getExCarte(){ return jeu.getExCarte(); }

    public static boolean isPossible(Matrice m, String type){ //Checkeur pour voir s'il est possible de faire coup
        return jeu.getCurrent_joueur().isPossible(m,type);
    }

    public static void Impossible(){ jeu.Impossible(); }//Action à traiter si le joueur ne peux pas faire de coup

    public static void addPiece(){
        jeu.getCurrent_joueur().addPiece();
    }

    public static int getNbTour(){ return jeu.getNbTour(); }

    //Méthode pour la création du jeu en mode multi
    public static void StartWithNomMulti(ArrayList<String> noms, ArrayList<String> titres){   jeu = JeuMulti.getInstance(noms, titres); }//Transfere des paramètres des textfields lors de la création de la partie

    //Méthode pour la fin du tour dans un mode solo
    public static void StartWithNomSolo(ArrayList<String> noms){   jeu = JeuSolo.getInstance(noms); }//Transfere des paramètres des textfields lors de la création de la partie

    //Méthode pour la fin du tour dans un mode solo
    public static void StartWithNomIA(int nb, String nom, String titre){   jeu = JeuIA.getInstance(nb, nom, titre); }//Transfere des paramètres des textfields lors de la création de la partie

    //Méthode pour la fin du tour dans un mode multi
    public static void EndTurnMulti(){
        assert jeu instanceof JeuMulti;
        ((JeuMulti) jeu).EndTurn();
    }

    //Méthode pour la fin du tour dans un mode solo
    public static void EndTurnSolo(){
        assert jeu instanceof JeuSolo;
        ((JeuSolo) jeu).EndTurn();

        while(jeu.getCurrent_carte() instanceof Carte_Monstre){//Si c'est une carte Monstre ne pas la poser ( elle est posée automatiquement)
            ((JeuSolo) jeu).EndTurn();
        }
    }

    //Méthode pour la fin du tour dans un mode IA
    public static void EndTurnIA() throws InterruptedException {
        assert jeu instanceof JeuIA;
        ((JeuIA) jeu).EndTurn();

    }

    public static void endGame(){ Vue.endGame(); }//Pour afficher la Vue de fin de partie

    public static boolean getFin(){ return jeu.getFin(); }

    public static Jeu getJeu(){
        return jeu;
    }

    public static void savePartie() throws IOException {
        SimpleDateFormat format= new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
        String date=format.format(new Date());
        String mode;

        if(jeu instanceof JeuMulti){ mode = "Multi"; }
        else if (jeu instanceof JeuSolo){ mode = "Solo"; }
        else { mode = "IA"; }
        FileOutputStream save=new FileOutputStream("saves\\"+mode+" - "+date+".ser"); //CrÃ©er ou remplace le fichier correspondant au chemin
        ObjectOutput oos=new ObjectOutputStream(save); //Permet l'ecriture dans le fichier en paramÃ¨tre

        oos.writeObject(jeu); //Sauvegarde sous forme binaire l'objet (ici il s'agit de 'jeu')
    }

    public static String[] getSaves(){
        File dossier=new File("saves\\"); //Dossier des sauvegardes
        File[] liste_saves=dossier.listFiles(); //Liste contenant tous les fichiers de sauvegardes
        assert liste_saves != null;

        assert(Objects.requireNonNull(liste_saves).length!=0); //On vÃ©rifie si il existe bien des sauvegardes

        String[] liste_string = new String[liste_saves.length]; //Liste de nom des sauvegardes
        int n=0;
        for(File i : liste_saves){ //Pour chaque sauvegardes prÃ©sente, on ajoute son nom dans le tableau de retour
            liste_string[n]=i.getName(); //On rÃ©cupÃ¨re le nom des fichiers de sauvegarde
            n++;
        }
        return liste_string; //On retourne la liste contenant les noms de chaques saves au format string
    }


    public static void chargerPartie(File save) throws IOException, ClassNotFoundException {
        FileInputStream charger=new FileInputStream("saves\\"+save); //RÃ©cupÃ©ration du fichier de sauvegarde
        ObjectInput ois=new ObjectInputStream(charger); //Permet la lecture dans le fichier Ã  charger

        jeu=(Jeu)ois.readObject(); //On attribue a la partie actuelle l'objet jeu chargÃ©
    }

}